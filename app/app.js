const yargs = require('yargs');

const daemonOption = {
  describe: 'Enable (true) or disable (false) auto-restarting',
  boolean: true,
  demand: false,
};

const argv = yargs
  .command('daemon', 'Turn on/off auto-restart', {
    enable: daemonOption,
  })
  .help()
  .alias('help', 'h').argv;



(async () => {
  if (argv._[0] === 'daemon') {
    const daemon = require('./models/daemon'); // eslint-disable-line global-require
    if (typeof argv.enable === 'boolean' && argv.enable === true) {
      console.log('Enable daemon functionality');
      try {
        await daemon.enable();
      } catch (e) {
        console.log(e);
      }
    } else if (typeof argv.enable === 'boolean' && argv.enable === false) {
      console.log('Disable daemon functionality');
      try {
        await daemon.disable();
      } catch (e) {
        console.log(e);
      }
    } else if (argv._[0] === 'daemon' && argv._[1] === 'start') {
      try {
        await daemon.start();
      } catch (e) {
        console.log(e);
      }
      try {
        const config = require('./models/config'); // eslint-disable-line global-require
        await config.parseConfigs();
        const interval = await config.returnKey(
          await config.returnKey(await config.returnConfigs('main'), 'daemon'),
          'interval'
        );
        setInterval(daemon.start, interval ? interval * 1000 : 10000);
      } catch (e) {
        console.log(e);
      }
    } else {
      console.log('Run it');
      try {
        await daemon.exec();
      } catch (e) {
        console.log(e);
      }
    }
  } else {
    const bot = require('./models/bot'); // eslint-disable-line global-require
    //
    //  Make import of model extends functions.
    //

    String.prototype.escapeNick = function () {
      const firstStep = this.replace('᠌᠌᠌᠌', ''); //remove empty string
      if (firstStep.length < 3) {
        return '** eBOT protection **';
      }
      return firstStep.replace(
        /\[(\w+)[^\]]*](.*?)\[\/\1]/g,
        '** eBOT protection **'
      );
    };

    try {
      if (!(await bot.running())) {
        await require('./models/config').parseConfigs(); // eslint-disable-line global-require
        const config = require('./models/config').configs; // eslint-disable-line global-require
        await require('./middlewares/database').dbConnection( // eslint-disable-line global-require
          config.main.database
        );
        const { buildConnection } = require('./models/teamspeak'); // eslint-disable-line global-require
        const connectionState = await buildConnection(config.main.connection, config.main.bot.channel);
        if (
          typeof connectionState !== 'undefined' &&
          connectionState === true
        ) {
          await require('./middlewares/connect').start(config.main); // eslint-disable-line global-require
          await require('./interfaces/handler').factory(config); // eslint-disable-line global-require
          console.log('success');
        } else {
          console.log('Something goes wrong');
          process.exit(1);
        }
        // if(await buildConnection(config.main) === true) {
        // //await new Promise(resolve => (async () => resolve(await require('./middlewares/connect').start(config.main))));
        // console.log(await require("./middlewares/connect").start(config.main));
        // setTimeout(async () => {await require('./interfaces/handler').factory(config)}, 1000)
        // } else {
        //   console.log('Something goes wrong');
        //    process.exit(1);
        // }
      }
    } catch (e) {
      console.log(e);
    }
  }
})();

// require('./schedule/reconnect');
