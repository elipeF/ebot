const factory = async config => {
  await require("./commands/commands").commandFactory(config.commands);
  await require("./events/events").eventExecuter(config.events);
  await require("./functions/functions").functionScheduler(config.functions);
};

module.exports = { factory };
