const { ts3 } = require("./../../models/teamspeak");
const executors = require("./executors/executors");
const moment = require("moment");
const socket = ts3();

const intervals = [];

const functionScheduler = async config => {
  for (const fct of Object.keys(config)) {
    if (executors.hasOwnProperty(fct)) {
      if (config[fct].enabled) {
        console.log(`Adding function ${fct} to scheduler`);
        try {
          await executors[fct].executer({ socket, config: config[fct] });
          intervals.push({
            function: executors[fct].executer,
            functionName: fct,
            meta: { socket, config: config[fct] },
            interval: config[fct].interval * 1000,
            lastExecuted: moment()
          });
        } catch (e) {
          console.log('Error during execution function: ' + fct + e)
        }
      }
    } else {
      console.log(`Nie ma takiej funkcji: ${fct}`);
    }
  }
};

const intervalsExecuter = async () => {
  for (const [index, interval] of intervals.entries()) {
    if (
      moment().diff(interval.lastExecuted, "milliseconds") > interval.interval
    ) {
      try {
        await interval.function(interval.meta);
        interval.lastExecuted = moment();
      } catch (e) {
        console.log('Error during execution function: ' + interval.functionName + e)
      }
    }
  }
  setTimeout(async () => {
    await intervalsExecuter();
  }, 100);
};

(() => {
  intervals.push({
    function: executors.usermonitor.executer,
    functionName: 'userMonitor',
    meta: { socket, timer: 1000 },
    interval: 5 * 1000,
    lastExecuted: moment()
  });
})();

setTimeout(async () => {
  await intervalsExecuter();
}, 100);

module.exports = { functionScheduler };
