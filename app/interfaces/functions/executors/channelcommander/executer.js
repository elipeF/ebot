const { PrivateChannel } = require("./../../../../models/db/privatechannel");
const moment = require("moment");
const uid = require("uuid/v1");
const nanoid = require("nanoid");

module.exports = {
  executer: async ({ socket, config }) => {
    //generate statistic
    let description = `[hr]
[center][size=20]Statystyka strefy prywatnej:[/size][/center]
[hr]\n`;
    for (i = 1; i <= 3; i++) {
      if (config.daysToRemove - i > 0) {
        let channelsDb;
        if (i === 1) {
          channelsDb = await PrivateChannel.find({
            validTo: { $lt: moment().add(i, "days").toDate() },
            status: 1
          });
        } else {
          channelsDb = await PrivateChannel.find({
            validTo: {
              $lt: moment().add(i, "days").toDate(),
              $gt: moment().add((i - 1), "days").toDate()
            },
            status: 1
          });
        }
        description =
          description +
          `[center][size=16]Kanały, które zostaną usunięte w ciągu ${i *
          24}h (${channelsDb.length}):[/size][/center]\n`;
        if (channelsDb.length > 0) {
          const channelsArray = [];
          for (const channel of channelsDb) {
            channelsArray.push(channel.number);
          }
          description =
            description +
            `[size=14][color=red]` +
            channelsArray.join(", ") +
            `[/color][/size]\n`;
        } else {
          description =
            description + `[size=14][color=red]Brak wyników[/color][/size]\n`;
        }
      }
    }
    const channelStats = await socket.getChannelByID(config.channelStats);
    await channelStats.edit({ channel_description: description });

    const channelsToDelete = await PrivateChannel.find({
      validTo: { $lt: moment().toDate() },
      status: 1
    });
    if (channelsToDelete.length > 0) {
      const tempNameObj = {}
      for (const toDel of channelsToDelete) {
        const channel = await socket.getChannelByID(toDel.cid);
        if (typeof channel === "undefined") {
          await PrivateChannel.findOneAndDelete({ _id: toDel._id });
        } else {
          //chanenel already taken need to remove
          const hash = uid();
          const getSubChannelsOfChannel = await socket.channelList({
            pid: channel.getCache().cid
          });
          if (getSubChannelsOfChannel.length > 0) {
            for (const sub of getSubChannelsOfChannel) {
              await sub.del(1);
            }
          }
          //clear channel groups

          try {
            const channelGroupOfChannel = await socket.channelGroupClientList(
              null,
              channel.getCache().cid
            );
            for (const toDel of channelGroupOfChannel) {
              await socket.setClientChannelGroup(
                config.channelGuest,
                toDel.cid,
                toDel.cldbid
              );
            }
          } catch (e) {
            // empty
          }

          let status = 0;

          if (config.reservations.enabled) {
            if (toDel.number <= config.reservations.toChannelId) {
              status = 2;
            }
          }
          try {
            await channel.edit({
              channel_name:
                status === 0
                  ? `${toDel.number}. Kanał wolny`
                  : `${toDel.number}. Kanał zarezerwowany`,
              channel_maxclients: 0,
              channel_description: config.channelDescriptionUrl.replace(
                "%hash%",
                hash
              ),
              channel_maxfamilyclients: 0,
              channel_flag_maxclients_unlimited: 0,
              channel_flag_maxfamilyclients_unlimited: 0,
              channel_flag_maxfamilyclients_inherited: 0,
              channel_password: '',
              channel_needed_talk_power: 0,
             // channel_icon_id: 0,
              channel_codec: 4,
              channel_codec_quality: 6
            });
          } catch (e) {
            //should calculate max payload length
            await channel.edit({
              channel_name: `${toDel.number}. eBOT {payload: ${nanoid(22-String(toDel.number).length)}}`,
              channel_maxclients: 0,
              channel_description: config.channelDescriptionUrl.replace(
                "%hash%",
                hash
              ),
              channel_maxfamilyclients: 0,
              channel_flag_maxclients_unlimited: 0,
              channel_flag_maxfamilyclients_unlimited: 0,
              channel_flag_maxfamilyclients_inherited: 0,
              channel_password: '',
              channel_needed_talk_power: 0,
             // channel_icon_id: 0,
              channel_codec: 4,
              channel_codec_quality: 6
            });
            tempNameObj[channel.getCache().cid] = status === 0
              ? `${toDel.number}. Kanał wolny`
              : `${toDel.number}. Kanał zarezerwowany`;
          }
          await PrivateChannel.updateOne({ _id: toDel._id }, { status, hash, owner: 0 });
        }
      }
      // sometimes channels name can't be set by first try, after checking all channels loop all and set correct name
      if(Object.keys(tempNameObj).length > 0) {
        for(const channelToBeFixed of Object.keys(tempNameObj)) {
          const findChannel = await socket.getChannelByID(channelToBeFixed);
          await findChannel.edit({channel_name: tempNameObj[channelToBeFixed]});
        }
      }
    }
    //get all channels from zone and check if last is free, loop and remove this channels
    const channelList = await socket.channelList({ pid: config.channelStats });
    let freeChannelRemoved = true;
    let channelPosition = channelList.length - 1;
    while (freeChannelRemoved) {
      const channel = channelList[channelPosition];
      const channelFromDb = await PrivateChannel.findOne({
        cid: channel.getCache().cid
      });
      if (channelFromDb !== null) {
        if (channelFromDb.status === 0) {
          await channel.del(1);
          await PrivateChannel.findOneAndDelete({ _id: channelFromDb._id });
          channelPosition = channelPosition - 1;
        } else {
          freeChannelRemoved = false;
        }
      } else {
        freeChannelRemoved = false;
      }
    }
  }
};
