const { Admin } = require("./../../../../models/db/admin");
const { User } = require("./../../../../models/db/user");
const moment = require("moment");
module.exports = {
  executer: async ({ socket, config }) => {
    const adminsDb = await Admin.getAllSorted({});
    //set as negetive value - negetive can't be get from Admin obj
    let lastSort = -1;
    let desc = `[hr]
[center][size=20]Spis Administracji[/size][/center]
[hr]`;
    for (const admin of adminsDb) {
      if (lastSort !== admin.sort) {
        desc =
          desc +
          `\n[center][img]${
            config.graphics[admin.sgid].header
          }[/img][/center]\n`;
        lastSort = admin.sort;
      }

      const adminServer = await socket.getClientByUID(admin.uniq);
      desc =
        desc +
        `\n[img]${config.graphics[admin.sgid].signet}[/img] [URL=client://0/${
          admin.uniq
        }~][color=${config.graphics[admin.sgid].color}][size=10]${
          admin.name
        }[/size][/color][/URL]\n`;
      if (typeof adminServer !== "undefined") {
        //online
        if (admin.l4) {
          desc = desc + "\n• Status: [b][color=grey]Urlop[/color][/b]";
          desc =
            desc +
            `\n• Urlop od: [b][color=orange]${moment(admin.l4date).format(
              "D/MM/YY H:mm"
            )}[/color][/b]`;
        } else {
          if(adminServer.getCache().client_away) {
            desc = desc + "\n• Status: [b][color=grey]Zaraz Wracam[/color][/b]";
          } else {
            desc = desc + "\n• Status: [b][color=green]Online[/color][/b]";
          }
        }
      } else {
        //ofline
        if (admin.l4) {
          desc = desc + "\n• Status: [b][color=grey]Urlop[/color][/b]";
          desc =
            desc +
            `\n• Urlop od: [b][color=orange]${moment(admin.l4date).format(
              "D/MM/YY H:mm"
            )}[/color][/b]`;
        } else {
          const userDb = await User.findOne({ uniq: admin.uniq });
          desc = desc + "\n• Status: [b][color=red]Offline[/color][/b]";
          if (userDb !== null) {
            desc =
              desc +
              `\n• Ostatnio widziany: [b][color=orange]${moment(
                userDb.lastConnection
              ).format("D/MM/YY H:mm")}[/color][/b]`;
          }
        }
      }
      desc = desc + "\n";
    }
    const channelToShow = await socket.getChannelByID(config.channelIdToShow);
    if (typeof channelToShow !== "undefined") {
      try {
        await channelToShow.edit({
          channel_description: desc
        });
      } catch (e) {
        //can't edit channel notify in future
      }
    } else {
      //no channel notify in future
    }
  }
};
