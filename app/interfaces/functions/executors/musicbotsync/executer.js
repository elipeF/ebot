const { api } = require('./../../../../models/musicbot/apiv1');
const { MusicBot } = require('./../../../../models/db/musicbot');
const { Guild } = require('./../../../../models/db/guild')

module.exports = {
  executer: async ({ socket, config }) => {
    const manyBotsOneGuild = {};
    let reBuildEvents = false;
    const musicbots = await api.fetchBots();
    if (musicbots.success) {
      let index = 0;
      const toDel = {};
      const inserts = [];
      for (const bot of musicbots.response) {
        toDel[bot.bot._id] = '';
        index++;
        const musicbot = await MusicBot.findOne({ botId: bot.bot._id });
        if (musicbot !== null) {
          delete toDel[bot.bot._id];
          let botChannel;
          let botName;
          let toClear = false;
          const name = bot.bot.name;
          const edit = {};
          if (musicbot.guild === 0) {
            botName = config.prefix + ' WOLNY #' + index;
          } else {
            const guild = await Guild.findOne({ sgid: musicbot.guild });
            if (guild === null) {
              botName = config.prefix + ' WOLNY #' + index;
              toClear = true;
            } else {
              if (manyBotsOneGuild.hasOwnProperty(musicbot.guild)) {
                manyBotsOneGuild[musicbot.guild]++;
                botName = config.prefix + ` ${guild.name} #` + manyBotsOneGuild[musicbot.guild];
              } else {
                manyBotsOneGuild[musicbot.guild] = 1;
                botName = config.prefix + ` ${guild.name} #` + 1;
              }
            }
          }
          if (botName !== name) {
            edit.name = botName
          }
          const channel = bot.bot.channel;
          if (musicbot.guild === 0) {
            botChannel = config.defaultChannel;
          } else {
            const guild = await Guild.findOne({ sgid: musicbot.guild });
            if (guild === null) {
              botChannel = config.defaultChannel;
            } else {
              const guild = await Guild.findOne({ sgid: musicbot.guild });
              if (guild !== null) {
                botChannel = guild.cidPass;
              }
            }
          }

          if (botChannel != channel) {
            edit.channel = botChannel
            reBuildEvents = true;
          }
          if (Object.keys(edit).length > 0) {
            await api.edit(bot.bot._id, edit);
          }
          if (toClear) {
            const botUsers = await api.getUsers(bot.bot._id);
            if (botUsers.success) {
              for (const user of botUsers.response.useruid) {
                await api.delUsers(bot.bot._id, user);
              }
              await MusicBot.updateOne({ botId: bot.bot._id }, { guild: 0, channel: Number(botChannel) });
            }
          }
        } else {
          delete toDel[bot.bot._id];
          inserts.push({
            botId: bot.bot._id,
            channel: config.defaultChannel,
            guild: 0,
          })
        }
      }
      if (reBuildEvents) {
        await require('./../../../events/events').reRunBuilder('guildmusicbot');
      }
      if (inserts.length > 0) {
        await new MusicBot(inserts).saveMany();
      }
      const toDelArr = Object.keys(toDel);
      if (toDelArr.length > 0) {
        for (const del of toDelArr) {
          await MusicBot.findOneAndDelete({ botId: del });
        }
      }
    } else{
      console.log('dupa123');
    }
  }
};
