const { User } = require("./../../../../models/db/user");

module.exports = {
  executer: async ({ socket, config }) => {
    const userList = await socket.clientList({ client_type: 0 });

    for (const user of userList) {
      let set = true;
      const clientGroups = user.getCache().client_servergroups;
      if ([clientGroups, config.noRegisterGroups].reduce((a, c) =>
        a.filter(i => c.includes(i))
      ).length === 0) {
        if (!clientGroups.includes(config.veryficationGroup)) {
          const userDb = await User.findOne({ cldbid: user.getDBID() });
          if (userDb !== null) {
            if (userDb.totalConnections < config.veryficationNeeds.connections) {
              set = false;
            }
            if (userDb.timeSpend < config.veryficationNeeds.timeTotal) {
              set = false;
            }
            if (userDb.timeSpendOnline < config.veryficationNeeds.timeOnline) {
              set = false;
            }
            if (set) {
              try {
                await user.serverGroupAdd(config.veryficationGroup);
                await user.message(config.userMessage.replace('%nick%', userDb.nickname));
              } catch (e) {
                console.log('WE GOT KURWA A PROBLEM HERE' + e);
              }
            }
          }
        }
      }
    }
  }
};
