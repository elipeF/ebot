const { User } = require("./../../../../models/db/user");
const moment = require("moment");
const isEqual = require('lodash.isequal');
module.exports = {
  executer: async ({ socket, timer }) => {
    const timerInSec = timer / 1000;
    const userList = await socket.clientList({ client_type: 0 });
    const inserts = [];
    for (const user of userList) {
      const userDb = await User.findOne({ cldbid: user.getDBID() });
      // console.log(userDb);
      const cache = user.getCache();
      try {
        const userInfo = await user.getInfo();
        if (userDb === null) {
          inserts.push({
            cldbid: user.getDBID(),
            uniq: user.getUID(),
            nickname: cache.client_nickname.escapeNick(),
            description: userInfo.client_description,
            totalConnections: userInfo.client_totalconnections,
            lastConnection: moment().toDate(),
            firstConnection: moment.unix(cache.client_created).toDate(),
            longestConnection: userInfo.connection_connected_time,
            timeSpend: userInfo.connection_connected_time / 1000,
            timeSpendOnline: userInfo.connection_connected_time / 1000,
            timeSpendAway: cache.client_idle_time / 1000,
            groups: cache.client_servergroups,
            ip: cache.connection_client_ip
          });
        } else {
          const objToUpdate = {};
          if (cache.client_nickname !== userDb.nickname) {
            objToUpdate.nickname = cache.client_nickname.escapeNick();
          }
          if (userInfo.client_description !== userDb.description) {
            objToUpdate.description = userInfo.client_description;
          }
          if (userInfo.client_totalconnections !== userDb.totalConnections) {
            objToUpdate.totalConnections = userInfo.client_totalconnections;
          }
          objToUpdate.lastConnection = moment().toDate();
          if (userInfo.connection_connected_time !== userDb.longestConnection) {
            objToUpdate.longestConnection = userInfo.connection_connected_time;
          }
          objToUpdate.timeSpend = userDb.timeSpend + timerInSec;

          if (cache.client_idle_time / 1000 > timerInSec * 10) {
            objToUpdate.timeSpendAway = userDb.timeSpendAway + timerInSec;
          } else {
            objToUpdate.timeSpendOnline = userDb.timeSpendOnline + timerInSec;
          }
          
          if (!isEqual(cache.client_servergroups, userDb.groups)) {
            objToUpdate.groups = cache.client_servergroups;
          }

          if (cache.connection_client_ip !== userDb.ip) {
            objToUpdate.ip = cache.connection_client_ip;
          }
          try {
            await User.updateOne({ _id: userDb._id }, objToUpdate);
          } catch (e) {
            console.log(e);
          }
        }
      } catch (e) {
       //user left during checking
      }
    }
    if (inserts.length > 0) {
      await new User(inserts).saveMany();
    }
  }
};
