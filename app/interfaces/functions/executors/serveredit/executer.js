module.exports = {
  executer: async ({ socket, config }) => {
    try {
      const serverInfo = await socket.serverInfo();
      await socket.serverEdit({ virtualserver_name: config.serverName.replace('%online%', (serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline)).replace('%total%', serverInfo.virtualserver_maxclients) })
    } catch (e) {
      console.log(e);
    }
  }
};
