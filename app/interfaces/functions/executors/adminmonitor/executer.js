const { Admin } = require("./../../../../models/db/admin");
const { User } = require("./../../../../models/db/user");
const moment = require("moment");
module.exports = {
  executer: async ({ socket, config }) => {
    const adminsDb = await Admin.find({});
    const adminServer = [];
    const adminsDbidsServer = [];
    let sort = 0;
    for (const adminGroup of config.groups) {
      sort++;
      const serverGroup = await socket.getServerGroupByID(adminGroup.sgid);
      if (typeof serverGroup !== "undefined") {
        const clientList = await serverGroup.clientList();
        if (clientList === null) {
        } else if (typeof clientList[Symbol.iterator] !== "function") {
          const userFromDb = await User.findOne({ cldbid: clientList.cldbid });
          if (userFromDb !== null) {
            if (!adminsDbidsServer.includes(clientList.cldbid)) {
              if (userFromDb.groups.includes(config.l4group)) {
                adminServer.push({
                  cldbid: clientList.cldbid,
                  name: userFromDb.nickname,
                  uniq: userFromDb.uniq,
                  sgid: adminGroup.sgid,
                  groupName: adminGroup.name,
                  sort,
                  l4: true,
                  l4date: moment().toDate()
                });
              } else {
                adminServer.push({
                  cldbid: clientList.cldbid,
                  uniq: userFromDb.uniq,
                  name: userFromDb.nickname,
                  sgid: adminGroup.sgid,
                  groupName: adminGroup.name,
                  sort,
                  l4: false
                });
              }
              adminsDbidsServer.push(clientList.cldbid);
            }
          }
        } else {
          for (const member of clientList) {
            const userFromDb = await User.findOne({ cldbid: member.cldbid });
            if (userFromDb !== null) {
              if (!adminsDbidsServer.includes(member.cldbid)) {
                if (userFromDb.groups.includes(config.l4group)) {
                  adminServer.push({
                    cldbid: member.cldbid,
                    name: userFromDb.nickname,
                    uniq: userFromDb.uniq,
                    sgid: adminGroup.sgid,
                    groupName: adminGroup.name,
                    sort,
                    l4: true,
                    l4date: moment().toDate()
                  });
                } else {
                  adminServer.push({
                    cldbid: member.cldbid,
                    uniq: userFromDb.uniq,
                    name: userFromDb.nickname,
                    sgid: adminGroup.sgid,
                    groupName: adminGroup.name,
                    sort,
                    l4: false
                  });
                }
                adminsDbidsServer.push(member.cldbid);
              }
            }
          }
        }
      }
    }
    if (adminServer.length > 0) {
      let toInsert = true;
      for (const [index, admin] of adminServer.entries()) {
        toInsert = true;
        for (const [indexDb, adminDb] of adminsDb.entries()) {
          if (admin.cldbid === adminDb.cldbid) {
            const toUpdate = {};
            if (admin.name !== adminDb.name) {
              toUpdate.name = admin.name;
            }
            if (admin.sgid !== adminDb.sgid) {
              toUpdate.sgid = admin.sgid;
            }
            if (admin.groupName !== adminDb.groupName) {
              toUpdate.groupName = admin.groupName;
            }
            if (admin.sort !== adminDb.sort) {
              toUpdate.sort = admin.sort;
            }
            if (admin.l4 !== adminDb.l4) {
              toUpdate.l4 = admin.l4;
              if (admin.l4) {
                toUpdate.l4date = admin.l4date;
              }
            }
            if (Object.keys(toUpdate).length > 0) {
              await Admin.updateOne({ _id: adminDb._id }, toUpdate);
            }
            adminsDb.splice(indexDb, 1);
            toInsert = false;
          }
        }
        if (toInsert) {
          await new Admin(admin).save();
        }
      }
      if (adminsDb.length > 0) {
        for (const adminDb of adminsDb) {
          await Admin.findOneAndDelete({ _id: adminDb._id });
        }
      }
    } else {
      // drop all from Admins should it?
    }
  }
};
