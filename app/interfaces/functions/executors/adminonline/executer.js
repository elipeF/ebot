const { Admin } = require("./../../../../models/db/admin");
module.exports = {
  executer: async ({ socket, config }) => {
    const adminsDb = await Admin.getAllSorted({ l4: false });
    let desc = `[hr]
[center][size=20]Dostępna administracja:[/size][/center]
[hr]`;
    let onlineCount = 0;
    for (const admin of adminsDb) {
      const adminServer = await socket.getClientByUID(admin.uniq);
      if (typeof adminServer !== "undefined") {
        if (adminServer.getCache().client_away === 0) {
          if (
            [
              adminServer.getCache().client_servergroups,
              config.disabledGroups
            ].reduce((a, c) => a.filter(i => c.includes(i))).length === 0
          ) {
            desc =
              desc +
              `\n• [URL=client://0/${admin.uniq}~] [ ${admin.groupName} ] ${
                admin.name
              }[/URL]`;
            onlineCount++;
          }
        }
      }
    }
    const channelToShow = await socket.getChannelByID(config.channelIdToShow);
    if (typeof channelToShow !== "undefined") {
      try {
        await channelToShow.edit({
          channel_name: config.channelName.replace(
            "%onlineCount%",
            onlineCount
          ),
          channel_description: desc
        });
      } catch (e) {
        //can't edit channel notify in future
      }
    } else {
      //no channel notify in future
    }
  }
};
