const { PrivateChannel } = require("./../../../../models/db/privatechannel");
const moment = require("moment");
const uid = require("uuid/v1");

module.exports = {
  executer: async ({ socket, config }) => {
    const privateChannels = await socket.channelList({ pid: config.zone });
    const serverInfo = await socket.serverInfo();
    const inserts = [];
    let numeration = 0;
    for (const channel of privateChannels) {
      const channelObj = {};
      numeration++;
      const channelDb = await PrivateChannel.findOne({
        cid: channel.getCache().cid
      });
      if (channelDb !== null) {
        const toUpdate = {};
        if (numeration !== channelDb.number) {
          toUpdate.number = numeration;
          toUpdate.hash = uid();
        }
        //check channel validity
        //check numeration
        if (channel.getCache().channel_name.match(/\d+\. /gms) === null) {
          const channelName =
            `${numeration}. ${channel.getCache().channel_name}`.length < 40
              ? `${numeration}. ${channel.getCache().channel_name}`
              : `${numeration}. Zbyt długa nazwa kanału`;

          if (channelName !== channel.getCache().channel_name) {
            channelObj.channel_name = channelName;
          }
          channelObj.channel_description = config.channelDescriptionUrl.replace(
            "%hash%",
            toUpdate.hash
          );
          await channel.edit(channelObj);
        } else {
          const number = Number(
            channel
              .getCache()
              .channel_name.match(/\d+\. /gms)[0]
              .replace(". ", "")
          );
          if (number !== numeration) {
            const channelName =
              channel
                .getCache()
                .channel_name.replace(`${number}. `, `${numeration}. `).length <
                40
                ? channel
                  .getCache()
                  .channel_name.replace(`${number}. `, `${numeration}. `)
                : `${numeration}. Zbyt długa nazwa kanału`;

            const channelObj = {};
            if (channelName !== channel.getCache().channel_name) {
              channelObj.channel_name = channelName;
            }
            channelObj.channel_description = config.channelDescriptionUrl.replace(
              "%hash%",
              toUpdate.hash
            );
            await channel.edit(channelObj);
          }
        }

        //do check only for private channels
        if (channelDb.status === 1) {
          //increase time online
          if (typeof channelDb.emptySince === "undefined") {
            await PrivateChannel.updateOne(
              { _id: channelDb._id },
              { emptySince: channel.getCache().seconds_empty }
            );
          } else {
            if (channel.getCache().seconds_empty <= channelDb.emptySince) {
              if (channel.getCache().seconds_empty === -1) {
                //still somebody on channel
                toUpdate.emptySince = channel.getCache().seconds_empty;
                toUpdate.validTo = moment(channelDb.validTo).add(
                  moment()
                    .add(config.channelValidity, "days")
                    .diff(moment(channelDb.validTo, "miliseconds")),
                  "milliseconds"
                ).toDate();
                //toUpdate.validTo =
                //console.log("ten kanał jest uzywany");
                //5 sec roznicy jako blad pomiarowy
              } else if (
                serverInfo.virtualserver_uptime -
                channel.getCache().seconds_empty <
                5
              ) {
                //console.log("blad pomiarowy");
              } else {
                toUpdate.emptySince = channel.getCache().seconds_empty;
                toUpdate.validTo = moment(channelDb.validTo).add(
                  moment()
                    .add(config.channelValidity, "days")
                    .diff(moment(channelDb.validTo, "miliseconds")),
                  "milliseconds"
                ).toDate();
                //console.log("ten kanał jest do aktualizacji");
              }
            } else {
              toUpdate.emptySince = channel.getCache().seconds_empty;
            }
          } // check if channel should be reservated
        } else if (channelDb.status === 0) {
          if (config.reservations.enabled) {
            if (numeration <= config.reservations.toChannelId) {
              toUpdate.status = 2;
              toUpdate.hash = uid();
              const channelName = `${numeration}. Kanał zarezerwowany`;

              if (channelName !== channel.getCache().channel_name) {
                channelObj.channel_name = channelName;
              }
              channelObj.channel_description = config.channelDescriptionUrl.replace(
                "%hash%",
                toUpdate.hash
              );
              await channel.edit(channelObj);
            }
          }
        }

        if (Object.keys(toUpdate).length > 0) {
          await PrivateChannel.updateOne({ _id: channelDb._id }, toUpdate);
        }
      } else {
        const group = await socket.getChannelGroupByID(config.channelAdmin);
        let owner = null;
        const hash = uid();
        try {
          const adminOfChannel = await group.clientList(channel.getCache().cid);
          owner = adminOfChannel[0].cldbid;
        } catch (e) {
          //getSubChannels no subchannels mean channel should be marked as free
          const subchannels = await socket.channelList({
            pid: channel.getCache().cid
          });
          if (subchannels.length > 0) {
            //some subchannels was found but nobody has rank, mared channel as ghost mean 0 id for the owner, channel validity is scheduled do be checked
            owner = 0;
          } else {
            //channel with no subchannel, no admin, clear all groups in channel and mark as free or reserved if configured
            try {
              const channelGroupOfChannel = await socket.channelGroupClientList(
                null,
                channel.getCache().cid
              );
              for (const toDel of channelGroupOfChannel) {
                await socket.setClientChannelGroup(
                  config.channelGuest,
                  toDel.cid,
                  toDel.cldbid
                );
              }
            } catch (e) {
              //no user with groups
            }
          }
        }

        if (owner !== null) {
          //check if channel name is ok
          if (channel.getCache().channel_name.match(/\d+\. /gms) === null) {
            const channelName =
              `${numeration}. ${channel.getCache().channel_name}`.length < 40
                ? `${numeration}. ${channel.getCache().channel_name}`
                : `${numeration}. Zbyt długa nazwa kanału`;

            if (channelName !== channel.getCache().channel_name) {
              channelObj.channel_name = channelName;
            }
            channelObj.channel_description = config.channelDescriptionUrl.replace(
              "%hash%",
              hash
            );
            await channel.edit(channelObj);
          } else {
            const number = Number(
              channel
                .getCache()
                .channel_name.match(/\d+\. /gms)[0]
                .replace(". ", "")
            );
            if (number !== numeration) {
              const channelName =
                channel
                  .getCache()
                  .channel_name.replace(`${number}. `, `${numeration}. `)
                  .length < 40
                  ? channel
                    .getCache()
                    .channel_name.replace(`${number}. `, `${numeration}. `)
                  : `${numeration}. Zbyt długa nazwa kanału`;

              if (channelName !== channel.getCache().channel_name) {
                channelObj.channel_name = channelName;
              }
              channelObj.channel_description = config.channelDescriptionUrl.replace(
                "%hash%",
                hash
              );
              await channel.edit(channelObj);
            } else {
              channelObj.channel_description = config.channelDescriptionUrl.replace(
                "%hash%",
                hash
              );
              await channel.edit(channelObj);
            }
          }
          inserts.push({
            cid: channel.getCache().cid,
            hash,
            number: numeration,
            owner,
            validTo: moment().add(config.channelValidity, "days").toDate(),
            emptySince: channel.getCache().seconds_empty,
            status: 1,
            createdAt: moment().toDate()
          });
        } else {
          //check if should be reserved
          if (
            config.reservations.enabled &&
            numeration <= config.reservations.toChannelId
          ) {
            //reserved
            const channelName =
              channel.getCache().channel_name ===
                `${numeration}. Kanał zarezerwowany`
                ? channel.getCache().channel_name
                : `${numeration}. Kanał zarezerwowany`;

            if (channelName !== channel.getCache().channel_name) {
              channelObj.channel_name = channelName;
            }
            channelObj.channel_description = config.channelDescriptionUrl.replace(
              "%hash%",
              hash
            );
            channelObj.channel_maxclients = 0;
            channelObj.channel_maxfamilyclients = 0;
            channelObj.channel_flag_maxclients_unlimited = 0;
            channelObj.channel_flag_maxfamilyclients_unlimited = 0;
            channelObj.channel_flag_maxfamilyclients_inherited = 0;
            await channel.edit(channelObj);

            inserts.push({
              cid: channel.getCache().cid,
              hash,
              number: numeration,
              status: 2,
              createdAt: moment().toDate()
            });
          } else {
            //free
            const channelName =
              channel.getCache().channel_name === `${numeration}. Kanał wolny`
                ? channel.getCache().channel_name
                : `${numeration}. Kanał wolny`;
            if (channelName !== channel.getCache().channel_name) {
              channelObj.channel_name = channelName;
            }
            channelObj.channel_description = config.channelDescriptionUrl.replace(
              "%hash%",
              hash
            );
            channelObj.channel_maxclients = 0;
            channelObj.channel_maxfamilyclients = 0;
            channelObj.channel_flag_maxclients_unlimited = 0;
            channelObj.channel_flag_maxfamilyclients_unlimited = 0;
            channelObj.channel_flag_maxfamilyclients_inherited = 0;
            await channel.edit(channelObj);
            inserts.push({
              cid: channel.getCache().cid,
              hash,
              number: numeration,
              status: 0,
              createdAt: moment().toDate()
            });
          }
        }
      }
    }
    if (inserts.length > 0) {
      await new PrivateChannel(inserts).saveMany();
    }
  }
};
