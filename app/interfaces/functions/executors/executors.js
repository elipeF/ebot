module.exports = {
    helloworld: {
        executer: require('./helloworld/executer').executer,
    },
    channelmonitor: {
        executer: require('./channelmonitor/executer').executer,
    },
    autoregister: {
        executer: require('./autoregister/executer').executer,
    },
    serveredit: {
        executer: require('./serveredit/executer').executer,
    },
    musicbotsync: {
        executer: require('./musicbotsync/executer').executer,
    },
    adminmonitor: {
        executer: require('./adminmonitor/executer').executer,
    },
    adminonline: {
        executer: require('./adminonline/executer').executer,
    },
    adminlist: {
        executer: require('./adminlist/executer').executer,
    },
    channelcommander: {
        executer: require('./channelcommander/executer').executer,
    },
    usermonitor: {
        executer: require('./usermonitor/executer').executer,
    },
}