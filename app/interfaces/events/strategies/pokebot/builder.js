const { executer } = require("./executer");

module.exports = {
  builder: async ({ config }) => {
    const relations = [];
    for (const single of config.schemas) {
      const obj = { [single.channel]: {
        function: executer,
        config: single
      } };
      relations.push(obj);
    }
    return relations;
  }
};