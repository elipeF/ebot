const moment = require("moment");

module.exports = {
  executer: async ({ socket, client, config }) => {
    //check if user is on server
    try {
      if (typeof (await client.getInfo()) === undefined) {
        return true;
      }

      //check if user is still in channel

      if (client.getCache().cid !== config.channel) {
        return true;
      }

      //check if user is in group needed to poke
      if (config.groupsNeededToPoke.length > 0) {
        const clientGroups = client.getCache().client_servergroups;
        if (
          [clientGroups, config.groupsNeededToPoke].reduce((a, c) =>
            a.filter(i => c.includes(i))
          ).length === 0
        ) {
          return socket.sendTextMessage(
            client.getID(),
            1,
            config.userMessages.noGroups
          );
        }
      }
      //check if on time to msg
      let dateWhenOpen = undefined;
      if (config.pokeWhen.hours.length > 0) {
        if (!config.pokeWhen.hours.includes(Number(moment().format("H")))) {
          if (config.pokeWhen.days.length > 0) {
            if (!config.pokeWhen.days.includes(Number(moment().format("e")))) {
              //inny dzien
              dateWhenOpen = findDayAndTime(
                moment(),
                config.pokeWhen.hours,
                config.pokeWhen.days
              );
            } else {
              const openIn = toTime(
                Number(moment().format("H")),
                config.pokeWhen.hours
              );
              if (
                openIn.diff(moment(), "minutes") >
                moment()
                  .endOf("day")
                  .diff(moment(), "minutes")
              ) {
                dateWhenOpen = findDayAndTime(
                  moment(),
                  config.pokeWhen.hours,
                  config.pokeWhen.days
                );
              } else {
                dateWhenOpen = openIn;
              }
            }
          } else {
            const openIn = toTime(
              Number(moment().format("H")),
              config.pokeWhen.hours
            );
            dateWhenOpen = openIn;
          }
        }
      }

      if (typeof dateWhenOpen === "undefined") {
        if (config.pokeWhen.days.length > 0) {
          if (!config.pokeWhen.days.includes(Number(moment().format("e")))) {
            dateWhenOpen = findDay(moment(), config.pokeWhen.days);
          }
        }
      }

      if (typeof dateWhenOpen !== "undefined") {
        const timeDiff = dateWhenOpen.diff(moment(), "minutes");
        return socket.sendTextMessage(
          client.getID(),
          1,
          config.userMessages.afterTime.replace(
            "%timeToOpen%",
            convertMinutes(timeDiff)
          )
        );
      }
      //get admins
      const admins = await getAdmins({
        socket,
        adminGroups: config.pokeTo,
        pokeIgnored: config.noPokeWhen
      });
      //check if admin on channel
      for (const admin of admins.list) {
        if (admin.cid === config.channel) {
          return true;
        }
      }
      //check if admin is available to poke
      if (admins.count === 0) {
        return socket.sendTextMessage(
          client.getID(),
          1,
          config.userMessages.noAdmins
        );
      }

      //poke admins now

      for (const admin of admins.list) {
        if (admin.poke) {
          const message =
            config.adminPoke.replace("%user%", `[URL=client://0/${client.getUID()}][color=red]${client.getCache().client_nickname}[/URL]`).length > 100
              ? config.adminPoke.replace("%user%", `[URL=client://0/${client.getUID()}][color=red]Ktoś[/URL]`)
              : config.adminPoke.replace("%user%", `[URL=client://0/${client.getUID()}][color=red]${client.getCache().client_nickname}[/URL]`);
          try {
            await socket.clientPoke(admin.clid, message);
          } catch (e) {
              //to large upr
          }
        }
      }

      //mesage to user
      await socket.sendTextMessage(
        client.getID(),
        1,
        config.userMessages.adminsAvaliable.replace("%adminCount%", admins.count)
      );

      return { interval: true, delay: config.pokeInterval };

      //repeate
    } catch (e) {
      console.log(e);
      return true;
    }
  }
};

const convertMinutes = num => {
  d = Math.floor(num / 1440); // 60*24
  h = Math.floor((num - d * 1440) / 60);
  m = Math.round(num % 60);

  if (d > 0) {
    return d + " d., " + h + " godz., " + m + " min.";
  } else {
    return h + " godz., " + m + " min.";
  }
};

const findDayAndTime = (now, hoursArray, daysArrays) => {
  for (i = now.format("d"); i <= 6; i++) {
    if (daysArrays.includes(i)) {
      return moment(
        `${moment().format("w")} ${i} ${hoursArray[0]}:00`,
        "w d H:m"
      );
    }
  }
  for (i = 0; i <= 6; i++) {
    if (daysArrays.includes(i)) {
      const now = moment(`${hoursArray[0]}:00`, "H:m");
      now.add(1, "w");
      now.add(i - now.format("d"), "d");
      return now;
    }
  }
};

const findDay = (now, daysArrays) => {
  for (i = now.format("d"); i <= 6; i++) {
    if (daysArrays.includes(i)) {
      return moment(`${moment().format("w")} ${i} 00:00`, "w d H:m");
    }
  }
  for (i = 0; i <= 6; i++) {
    if (daysArrays.includes(i)) {
      const now = moment(`00:00`, "H:m");
      now.add(1, "w");
      now.add(i - now.format("d"), "d");
      return now;
    }
  }
};

const toTime = (now, array) => {
  for (i = now; i <= 23; i++) {
    if (array.includes(i)) {
      return moment(`${i}:00`, "H:m");
    }
  }
  for (i = 0; i <= 23; i++) {
    if (array.includes(i)) {
      const now = moment(`${i}:00`, "H:m");
      now.add(1, "day");
      return now;
    }
  }
};

const getAdmins = async ({ socket, adminGroups, pokeIgnored }) => {
  const admins = { list: [], count: 0 };
  for (const admin of adminGroups) {
    const members = await socket.serverGroupClientList(admin);
    if (members === null) {
    } else if (typeof members.length === "undefined") {
      const user = await socket.getClientByUID(
        members.client_unique_identifier
      );
      if (typeof user !== "undefined") {
        const userGroups = user.getCache().client_servergroups;
        if (
          [userGroups, pokeIgnored].reduce((a, c) =>
            a.filter(i => c.includes(i))
          ).length > 0
        ) {
          admins.list.push({
            clid: user.getID(),
            cid: user.getCache().cid,
            poke: false
          });
        } else {
          admins.list.push({
            clid: user.getID(),
            cid: user.getCache().cid,
            poke: true
          });
          admins.count++;
        }
      }
    } else {
      for (const member of members) {
        const user = await socket.getClientByUID(
          member.client_unique_identifier
        );
        if (typeof user !== "undefined") {
          const userGroups = user.getCache().client_servergroups;
          if (
            [userGroups, pokeIgnored].reduce((a, c) =>
              a.filter(i => c.includes(i))
            ).length > 0
          ) {
            admins.list.push({
              clid: user.getID(),
              cid: user.getCache().cid,
              poke: false
            });
          } else {
            admins.list.push({
              clid: user.getID(),
              cid: user.getCache().cid,
              poke: true
            });
            admins.count++;
          }
        }
      }
    }
  }
  return admins;
};
