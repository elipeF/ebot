const { Guild } = require("./../../../../models/db/guild");
const { User } = require("./../../../../models/db/user");
const moment = require("moment");
module.exports = {
  executer: async ({ socket, client, config }) => {
    try {
      const group = await socket.getServerGroupByID(config.sgid);
      if (typeof group !== "undefined") {
        const online = [];
        const offline = [];
        const members = await group.clientList();
        if (members === null) {
        } else if (typeof members[Symbol.iterator] !== "function") {
          const onServer = await socket.getClientByUID(
            members.client_unique_identifier
          );
          if (typeof onServer !== "undefined") {
            online.push({
              nickname: members.client_nickname.escapeNick(),
              cldbid: members.cldbid,
              uniq: members.client_unique_identifier
            });
          } else {
            offline.push({
              nickname: members.client_nickname.escapeNick(),
              cldbid: members.cldbid,
              uniq: members.client_unique_identifier
            });
          }
        } else {
          for (const member of members) {
            const onServer = await socket.getClientByUID(
              member.client_unique_identifier
            );
            if (typeof onServer !== "undefined") {
              online.push({
                nickname: member.client_nickname.escapeNick(),
                cldbid: member.cldbid,
                uniq: member.client_unique_identifier
              });
            } else {
              offline.push({
                nickname: member.client_nickname.escapeNick(),
                cldbid: member.cldbid,
                uniq: member.client_unique_identifier
              });
            }
          }
        }
        //edit channel
        let description = `[hr]
[center][size=20]Status gildii [${config.name}]
(${online.length}/${offline.length + online.length})[/size][/center]
[hr]`;
        for (const on of online) {
          description =
            description +
            `\n[URL=client:///${on.uniq}][color=green]${
              on.nickname
            }[/color][/URL]`;
        }
        for (const off of offline) {
          const userDb = await User.findOne({ cldbid: off.cldbid });
          if (userDb !== null) {
            description =
              description +
              `\n[color=grey]${off.nickname} [${moment(
                userDb.lastConnection
              ).format("D/MM/YY H:mm")}][/color]`;
          } else {
            description = description + `\n[color=grey]${off.nickname}[/color]`;
          }
        }
        const channel = await socket.getChannelByID(config.cidOnline);
        if (typeof channel !== "undefined") {
          try {
            const channelName =
              typeof channel.getCache().channel_topic === "undefined"
                ? `· Online z ${config.name}: ${
                    online.length
                  } / ${offline.length + online.length}`
                : channel
                    .getCache()
                    .channel_topic.replace("NAZWA", config.name)
                    .replace("x ", online.length)
                    .replace(" y", offline.length + online.length);
            await channel.edit({
              channel_name: channelName,
              channel_description: description
            });
          } catch (e) {}
        }
      } else {
        await Guild.findOneAndDelete({ sgid: config.sgid });
        await require('./../../events').reRunBuilder('guildonline');
        await require('./../../events').reRunBuilder('guildaddremove');
      }
    } catch (e) {
      console.log(e);
    }
    return true;
  }
};
