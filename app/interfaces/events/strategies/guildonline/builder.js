const { executer } = require("./executer");
const { Guild } = require("./../../../../models/db/guild");

module.exports = {
  builder: async ({ config }) => {
    const relations = [];
    const guilds = await Guild.find({});
    for (const guild of guilds) {
      const obj = {
        [guild.sgid]: {
          function: executer,
          config: guild
        }
      };
      relations.push(obj);
    }
    return relations;
  }
};
