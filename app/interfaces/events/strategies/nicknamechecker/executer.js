module.exports = {
  executer: async ({ socket, client, config }) => {
    for (const word of config.badWords) {
      if (client.getCache().client_nickname.toLowerCase().escapeNick().includes(word)) {
        return client.kickFromServer(`Twoj nick zawiera zakazane slowo: '${word}'`)
      }
    }
  }
};
