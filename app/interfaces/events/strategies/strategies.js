module.exports = {
  onchannel: {
    pokebot: {
      builder: require("./pokebot/builder").builder
    },
    getprivatechannel: {
      builder: require("./getprivatechannel/builder").builder
    },
    guildaddremove: {
      builder: require("./guildaddremove/builder").builder
    },
    guildmusicbot: {
      builder: require("./guildmusicbot/builder").builder
    }
  },
  ingroup: {
    guildonline: {
      builder: require("./guildonline/builder").builder
    }
  },
  onjoin: {
    record: {
      builder: require("./record/builder").builder
    }
  },
  onnamechange: {
    nicknamechecker: {
      builder: require("./nicknamechecker/builder").builder
    }
  }
};
