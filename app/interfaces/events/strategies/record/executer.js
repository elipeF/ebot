// const memory = require('./memory').memory();
const moment = require("moment");
const { Record } = require('./../../../../models/db/record');

module.exports = {
  executer: async ({ socket, config }) => {
    const lastRecord = await Record.findLast({ sort: { $natural: -1 } });
    const serverInfo = await socket.serverInfo();
    if (lastRecord === null) {
      const date = moment();
      await new Record({
        value: (serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline),
        displayed: false,
        establishedAt: date.toDate()
      }).save();
      const channel = await socket.getChannelByID(config.channelToDisplayRecord);
      if (typeof channel !== "undefined") {
        await channel.edit({
          channel_name: config.nameToDisplay.replace('%record%', (serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline)),
          channel_description: `[hr]
[center][size=20]Aktualny rekord:[/size][/center]
[hr]
[center][color=green][b][u][size=12]Rekord ustanowiony:[/u][/color][size=11][b]

Rekord: [color=blue]${(serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline)} osób [/color]
Data: [color=blue]${date.format('DD.MM.YYYY HH:mm')}[/color][/size][/center]
[hr]`,
        });
      }
    } else {
      const parsedDate = moment(lastRecord.establishedAt);
      if (parsedDate.format('DD.MM') === moment().format('DD.MM')) {
        if ((serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline) > lastRecord.value) {
          const date = moment();
          await Record.updateOne({ _id: lastRecord._id }, {
            value: (serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline),
            establishedAt: date.toDate()
          })
          //desc
          let desc = `[hr]
[center][size=20]Aktualny rekord:[/size][/center]
[hr]
[center][color=green][b][u][size=12]Rekord ustanowiony:[/u][/color][size=11][b]

Rekord: [color=blue]${(serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline)} osób [/color]
Data: [color=blue]${date.format('DD.MM.YYYY HH:mm')}[/color][/size][/center]
[hr]`;
          if (config.recordHistoryCount > 0) {
            desc = desc + `\n[size=18] Historia rekordów:[/size]\n\n`;
            const dbRecords = await Record.findXRecords({ displayed: true }, config.recordHistoryCount);
            for (const record of dbRecords) {
              desc = desc + `[b]Rekord: [color=red]${record.value}[/color] dnia: ${moment(record.establishedAt).format('DD.MM.YYYY HH:mm')}[/b]\n`
            }
          }
          const channel = await socket.getChannelByID(config.channelToDisplayRecord);
          if (typeof channel !== "undefined") {
            await channel.edit({
              channel_name: config.nameToDisplay.replace('%record%', (serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline)),
              channel_description: desc
            });
          }
        }
      } else {
        if (!lastRecord.displayed) {
          if ((serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline) > lastRecord.value) {
            const date = moment();
            await new Record({
              value: (serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline),
              displayed: false,
              establishedAt: date.toDate()
            }).save();
            await Record.updateOne({ _id: lastRecord._id }, { displayed: true });
            let desc = `[hr]
[center][size=20]Aktualny rekord:[/size][/center]
[hr]
[center][color=green][b][u][size=12]Rekord ustanowiony:[/u][/color][size=11][b]

Rekord: [color=blue]${(serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline)} osób [/color]
Data: [color=blue]${date.format('DD.MM.YYYY HH:mm')}[/color][/size][/center]
[hr]`;
            if (config.recordHistoryCount > 0) {
              desc = desc + `\n[size=18] Historia rekordów:[/size]\n\n`;
              const dbRecords = await Record.findXRecords({ displayed: true }, config.recordHistoryCount);
              for (const record of dbRecords) {
                desc = desc + `[b]Rekord: [color=red]${record.value}[/color] dnia: ${moment(record.establishedAt).format('DD.MM.YYYY HH:mm')}[/b]\n`
              }
            }
            const channel = await socket.getChannelByID(config.channelToDisplayRecord);
            if (typeof channel !== "undefined") {
              await channel.edit({
                channel_name: config.nameToDisplay.replace('%record%', (serverInfo.virtualserver_clientsonline - serverInfo.virtualserver_queryclientsonline)),
                channel_description: desc
              });
            }
          }
        }

      }
      // if (lastRecord === null) {
      //   await new Record({
      //     value: 0,
      //     displayed: false,
      //     establishedAt: moment().toDate()
      //   }).save();
      // } else {

      // }

      //  await new Record({
      //   value: 0,
      //   displayed: false,
      //   establishedAt: moment().toDate()
      // }).save();
      // console.log(lastRecord);

      //todo forEach for last X records,
      //check if in last X records is record with displayed false property
      // if yes check if day is diffrent from now
      // then replace false to true
      // display in channel description
      // maybe should it validate if description change?
      // or it should check for all changes in the if statement before

    }
  }
};
