const { executer } = require("./executer");

module.exports = {
  builder: async ({ config }) => {
    const relations = [];
    const obj = {
      record: {
        function: executer,
        config: config
      }
    };
    relations.push(obj);
    return relations;
  }
};
