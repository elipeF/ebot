const { executer } = require("./executer");
const { MusicBot } = require("./../../../../models/db/musicbot");
const { Guild } = require("./../../../../models/db/guild");
const { api } = require("./../../../../models/musicbot/apiv1")

module.exports = {
  builder: async ({ config }) => {
    const relations = [];
    const bots = await MusicBot.find({ guild: { $ne: 0 } });
    for (const bot of bots) {
      const guild = await Guild.findOne({ sgid: bot.guild })
      if (guild !== null) {
        const botName = await api.fetchBot(bot.botId);
        bot.name = botName.success ? botName.response.name : 'Bot muzyczny TS3.BLACK';
        bot.passChannel = guild.cidPass
        bot.apiUrl = config.apiUrl;
        bot.messages = config.messages;
        const obj = {
          [bot.channel]: {
            function: executer,
            config: bot
          }
        };
        relations.push(obj);
      } else {
        await MusicBot.updateOne({ _id: bot._id }, { guild: 0 });
      }
    }
    return relations;
  }
};
