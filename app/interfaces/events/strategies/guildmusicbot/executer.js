const { api } = require("./../../../../models/musicbot/apiv1");
const { User } = require("./../../../../models/db/user");
const { MusicBot } = require('./../../../../models/db/musicbot');
module.exports = {
  executer: async ({ socket, client, config }) => {
    try {
      const access = await api.getUsers(config.botId);
      if (access.success) {
        if (access.response.useruid.includes(client.getUID())) {
          const req = await api.delUsers(config.botId, client.getUID());
          if (req.success) {
            await client.move(config.passChannel);
            await client.poke(config.messages.accessRevoke);
            let desc = `
[hr]
[center][size=20]Uprawnienia do bota:
[/size][size=14][color=orange]${config.name}[/color][/size][/center]
[hr]`;
            if (req.response.useruid.length > 0) {
              for (const user of req.response.useruid) {
                const userDb = await User.findOne({ uniq: user });
                if (userDb !== null) {
                  desc = desc + `\n[URL=client://1/${user}]${userDb.nickname}[/URL]`;
                }
              }
            }
            const channel = await socket.getChannelByID(config.channel);
            if (typeof channel !== "undefined") {
              await channel.edit({ channel_description: desc })
            } else {
              await MusicBot.updateOne({ botId: config.botId }, { guild: 0 })
            }
          } else {
            await client.move(config.passChannel);
            await client.poke('Wystąpił błąd. Spróbuj ponownie później.')
          }
        } else {
          const req = await api.setUsers(config.botId, client.getUID());
          if (req.success) {
            await client.move(config.passChannel);
            await client.poke(config.messages.accessGain);
            let desc = `
[hr]
[center][size=20]Uprawnienia do bota:
[/size][size=14][color=orange]${config.name}[/color][/size][/center]
[hr]`;
            if (req.response.useruid.length > 0) {
              for (const user of req.response.useruid) {
                const userDb = await User.findOne({ uniq: user });
                if (userDb !== null) {
                  desc = desc + `\n[URL=client://1/${user}]${userDb.nickname}[/URL]`;
                }
              }
            }
            const channel = await socket.getChannelByID(config.channel);
            if (typeof channel !== "undefined") {
              await channel.edit({ channel_description: desc })
            } else {
              await MusicBot.updateOne({ botId: config.botId }, { guild: 0 })
            }
          } else {
            await client.move(config.passChannel);
            await client.poke('Wystąpił błąd. Spróbuj ponownie później.')
          }
        }
      } else {
        await client.move(config.passChannel);
        await client.poke('Wystąpił błąd. Spróbuj ponownie później.')
      }
    } catch (e) {
      try {
        await client.move(config.passChannel);
        await client.poke('Wystąpił błąd. Spróbuj ponownie później.')
      } catch (e) {

      }
      console.log(e);
    }
    return true;
  }
};
