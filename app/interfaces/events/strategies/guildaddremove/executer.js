module.exports = {
  executer: async ({ socket, client, config }) => {
    try {
      if (
        !client.getCache().client_servergroups.includes(Number(config.sgid))
      ) {
        await client.serverGroupAdd(config.sgid);
        await client.move(config.cidPass);
        await client.poke(`Grupa nadana!`);
      } else {
        await client.serverGroupDel(config.sgid);
        await client.kickFromChannel();
        await client.poke(`Grupa zabrana!`);
      }
    } catch (e) {
      console.log(e);
    }
    return true;
  }
};
