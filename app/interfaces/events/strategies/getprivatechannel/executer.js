const { User } = require("./../../../../models/db/user");
const { PrivateChannel } = require("./../../../../models/db/privatechannel");
const uid = require("uuid/v1");
const moment = require("moment");

module.exports = {
  executer: async ({ socket, client, config }) => {
    // validating user need here
  //   if(config.userNeeds.validating) {
  //     const userDb = await User.findOne({ cldbid: client.getDBID() });

  //   if (userDb === null) {
  //     //todo

  //     return client.message(
  //       "Bot jest w trakcie analizy Twojego ID. Sprobuj za chwile"
  //     );
  //   }

  //   if (userDb !== null) {
  //     //validate
  //   }
  // }

    //check if userIsOwner
    const userChannel = await PrivateChannel.findOne({ owner: client.getDBID() });
    if (userChannel !== null) {
      await client.move(userChannel.cid);
      await client.message(
        config.userMessages.userHasChannel
          .replace("%userName%", client.getCache().client_nickname)
          .replace("%channelNumber%", userChannel.number)
      );
      return true;
    }
    let free = true;
    //check if free channel
    while (free) {
      const channel = await PrivateChannel.findOne({ status: 0 });
      const hash = uid();
      if (channel !== null) {
        //wolny kanał
        const channelServer = await socket.getChannelByID(channel.cid);
        if (typeof channelServer !== "undefined") {
          free = false;
          const channelName =
            `${channel.number}. kanał uzytkownika - ${
              client.getCache().client_nickname
            }`.length < 40
              ? `${channel.number}. kanał uzytkownika - ${
                  client.getCache().client_nickname
                }`
              : `${channel.number}. nowy kanał uzytkownika`;
          await channelServer.edit({
            channel_name: channelName,
            channel_description: config.newChannelSettings.channelDescriptionUrl.replace(
              "%hash%",
              hash
            ),
            channel_maxclients: -1,
            channel_maxfamilyclients: 0,
            channel_flag_maxclients_unlimited: 1,
            channel_flag_maxfamilyclients_unlimited: 1,
            channel_flag_maxfamilyclients_inherited: 0,
            channel_flag_permanent: 1
          });
          for (i = 1; i <= config.newChannelSettings.subChannelsCount; i++) {
            await socket.channelCreate(`${i}. Podkanał`, {
              cpid: channel.cid,
              channel_maxclients: -1,
              channel_maxfamilyclients: 0,
              channel_flag_maxclients_unlimited: 1,
              channel_flag_maxfamilyclients_unlimited: 1,
              channel_flag_maxfamilyclients_inherited: 0,
              channel_flag_permanent: 1
            });
          }
          await socket.setClientChannelGroup(
            config.channelAdmin,
            channel.cid,
            client.getDBID()
          );
          await PrivateChannel.updateOne(
            { _id: channel._id },
            {
              hash,
              owner: client.getDBID(),
              validTo: moment()
                .add(config.newChannelSettings.channelValidy, "days")
                .toDate(),
              status: 1,
              createdAt: moment().toDate()
            }
          );
          await client.move(channel.cid);
        } else {
          await PrivateChannel.findOneAndDelete({ _id: channel._id });
        }
      } else {
        free = false;
        //stworz kanał
        //pobierz liste kanałow by znalesc ostatni numer,
        const channelList = await socket.channelList({ pid: config.zone });
        let number;
        if (channelList.length === 0) {
          number = 1;
        } else {
          const channel = channelList[channelList.length - 1];
          number =
            channel.getCache().channel_name.match(/\d+\. /gms) !== null
              ? Number(
                  channel
                    .getCache()
                    .channel_name.match(/\d+\. /gms)[0]
                    .replace(". ", "")
                ) + 1
              : Number(channelList.length) + 1;
        }

        const channelName =
          `${number}. kanał uzytkownika - ${client.getCache().client_nickname}`
            .length < 40
            ? `${number}. kanał uzytkownika - ${
                client.getCache().client_nickname
              }`
            : `${number}. nowy kanał uzytkownika`;
        const newChannel = await socket.channelCreate(channelName, {
          cpid: config.zone,
          channel_description: config.newChannelSettings.channelDescriptionUrl.replace(
            "%hash%",
            hash
          ),
          channel_maxclients: -1,
          channel_maxfamilyclients: 0,
          channel_flag_maxclients_unlimited: 1,
          channel_flag_maxfamilyclients_unlimited: 1,
          channel_flag_maxfamilyclients_inherited: 0,
          channel_flag_permanent: 1
        });

        for (i = 1; i <= config.newChannelSettings.subChannelsCount; i++) {
          await socket.channelCreate(`${i}. Podkanał`, {
            cpid: newChannel.getCache().cid,
            channel_maxclients: -1,
            channel_maxfamilyclients: 0,
            channel_flag_maxclients_unlimited: 1,
            channel_flag_maxfamilyclients_unlimited: 1,
            channel_flag_maxfamilyclients_inherited: 0,
            channel_flag_permanent: 1
          });
        }
        await socket.setClientChannelGroup(
          config.channelAdmin,
          newChannel.getCache().cid,
          client.getDBID()
        );
        await new PrivateChannel({
          cid: newChannel.getCache().cid,
          hash,
          number,
          owner: client.getDBID(),
          validTo: moment()
            .add(config.newChannelSettings.channelValidy, "days")
            .toDate(),
          status: 1,
          createdAt: moment().toDate()
        }).save();
        await client.move(newChannel.getCache().cid);
        //console.log(channelList.length);
      }

      return true;
    }
  }
};
