const { ts3 } = require("./../../models/teamspeak");
const strategies = require("./strategies/strategies");
const socket = ts3();
const moment = require("moment");

const relations = {
  onchannel: {},
  ingroup: {},
  onjoin: {},
  onnamechange: {}
};
const blockedChannels = {};
const builders = {};
const intervals = [];

const eventExecuter = async config => {
  const tempObj = {};
  const checks = Object.keys(relations).length;
  for (const relation of Object.keys(relations)) {
    for (const fct of Object.keys(config)) {
      if (strategies[relation].hasOwnProperty(fct)) {
        if (config[fct].enabled) {
          console.log(`Listening to the ${fct} event`);
          const relationToAdd = await strategies[relation][fct].builder({
            config: config[fct]
          });
          builders[fct] = {
            builder: strategies[relation][fct].builder,
            type: relation,
            config: config[fct]
          };
          for (const rel of relationToAdd) {
            const key = Object.keys(rel)[0];
            relations[relation][key] = rel[key];
            relations[relation][key].functionName = fct;
          }
        }
      } else {
        if (tempObj.hasOwnProperty(fct)) {
          tempObj[fct] += 1;
        } else {
          tempObj[fct] = 1;
        }
      }
    }
  }
  for (const check of Object.keys(tempObj)) {
    if (tempObj[check] === checks) {
      console.log(`Nie ma takiego eventu: ${check}`);
    }
  }
};

socket.on("clientdisconnect", async ev => {
  const client = ev.client;
  if (typeof client.client_servergroups !== "undefined") {
    for (const event of Object.keys(relations.ingroup)) {
      try {
        if (client.client_servergroups.includes(Number(event))) {
          await relations.ingroup[event].function({
            socket,
            client,
            config: relations.ingroup[event].config
          });
        }
      } catch (e) {
        console.log(`Error in event: ${relations.ingroup[event].functionName} ${e}`)
      }
    }
  }
});

socket.on("clientconnect", async ev => {
  const client = ev.client;

  if (!client.isQuery()) {
    for (const event of Object.keys(relations.onjoin)) {
      try {
        await relations.onjoin[event].function({
          socket,
          client,
          config: relations.onjoin[event].config
        });
      } catch (e) {
        console.log(`Error in event: ${relations.onjoin[event].functionName} ${e}`)
      }
    }



    client.on("update#client_nickname", async change => {
      for (const event of Object.keys(relations.onnamechange)) {
        try {
          await relations.onnamechange[event].function({
            socket,
            client,
            config: relations.onnamechange[event].config
          });
        } catch (e) {
          console.log(`Error in event: ${relations.onnamechange[event].functionName} ${e}`)
        }
      }
    });

    for (const event of Object.keys(relations.onnamechange)) {
      try {
        await relations.onnamechange[event].function({
          socket,
          client,
          config: relations.onnamechange[event].config
        });
      } catch (e) {
        console.log(`Error in event: ${relations.onnamechange[event].functionName} ${e}`)
      }
    }


    for (const event of Object.keys(relations.ingroup)) {
      try {
        if (client.getCache().client_servergroups.includes(Number(event))) {
          await relations.ingroup[event].function({
            socket,
            client,
            config: relations.ingroup[event].config
          });
        }
      } catch (e) {
        console.log(`Error in event: ${relations.ingroup[event].functionName} ${e}`)
      }
    }


    client.on("update#client_servergroups", async change => {
      for (const event of Object.keys(relations.ingroup)) {
        try {
          if (
            (!change.from.includes(Number(event)) &&
              change.to.includes(Number(event))) ||
            (change.from.includes(Number(event)) &&
              !change.to.includes(Number(event)))
          ) {
            await relations.ingroup[event].function({
              socket,
              client,
              config: relations.ingroup[event].config
            });
          }
        } catch (e) {
          console.log(`Error in event: ${relations.ingroup[event].functionName} ${e}`)
        }
      }
    });

    client.on("update#cid", async change => {
      if (relations.onchannel.hasOwnProperty(change.to)) {
        try {
          if (
            !blockedChannels.hasOwnProperty(
              relations.onchannel[change.to].config.channel
            )
          ) {
            const resp = await relations.onchannel[change.to].function({
              socket,
              client,
              config: relations.onchannel[change.to].config
            });
            if (resp !== null && resp.hasOwnProperty("interval")) {
              if (resp.interval === true) {
                blockedChannels[
                  relations.onchannel[change.to].config.channel
                ] = null;
                intervals.push({
                  function: relations.onchannel[change.to].function,
                  meta: {
                    socket,
                    client,
                    config: relations.onchannel[change.to].config
                  },
                  channel: relations.onchannel[change.to].config.channel,
                  interval: resp.delay * 1000,
                  lastExecuted: moment()
                });
              }
            }
          }
        } catch (e) {
          console.log(`Error in event: ${relations.onchannel[change.to].functionName} ${e}`)
        }
      }
    });
  }
});

(async () => {
  const clients = await socket.clientList({ client_type: 0 });
  for (const event of Object.keys(relations.onjoin)) {
    try {
      await relations.onjoin[event].function({
        socket,
        config: relations.onjoin[event].config
      });
    } catch (e) {
      console.log(`Error in event: ${relations.onjoin[event].functionName} ${e}`)
    }
  }
  for (const client of clients) {
    if (relations.onchannel.hasOwnProperty(client.getCache().cid)) {
      try {
        if (
          !blockedChannels.hasOwnProperty(
            relations.onchannel[client.getCache().cid].config.channel
          )
        ) {
          const resp = await relations.onchannel[
            client.getCache().cid
          ].function({
            socket,
            client,
            config: relations.onchannel[client.getCache().cid].config
          });
          if (resp !== null && resp.hasOwnProperty("interval")) {
            if (resp.interval === true) {
              blockedChannels[
                relations.onchannel[client.getCache().cid].config.channel
              ] = null;
              intervals.push({
                function: relations.onchannel[client.getCache().cid].function,
                meta: {
                  socket,
                  client,
                  config: relations.onchannel[client.getCache().cid].config
                },
                channel:
                  relations.onchannel[client.getCache().cid].config.channel,
                interval: resp.delay * 1000,
                lastExecuted: moment()
              });
            }
          }
        }
      } catch (e) {
        console.log(`Error in event: ${relations.onchannel[client.getCache().cid].functionName} ${e}`)
      }
    }
    for (const event of Object.keys(relations.onnamechange)) {
      try {
        await relations.onnamechange[event].function({
          socket,
          client,
          config: relations.onnamechange[event].config
        });
      } catch (e) {
        console.log(`Error in event: ${relations.onnamechange[event].functionName} ${e}`)
      }
    }


    client.on("update#client_nickname", async change => {
      for (const event of Object.keys(relations.onnamechange)) {
        try {
          await relations.onnamechange[event].function({
            socket,
            client,
            config: relations.onnamechange[event].config
          });
        } catch (e) {
          console.log(`Error in event: ${relations.onnamechange[event].functionName} ${e}`)
        }
      }
    });

    client.on("update#client_servergroups", async change => {
      for (const event of Object.keys(relations.ingroup)) {
        if (
          (!change.from.includes(Number(event)) &&
            change.to.includes(Number(event))) ||
          (change.from.includes(Number(event)) &&
            !change.to.includes(Number(event)))
        ) {
          try {
            await relations.ingroup[event].function({
              socket,
              client,
              config: relations.ingroup[event].config
            });
          } catch (e) {
            console.log(`Error in event: ${relations.ingroup[event].functionName} ${e}`)
          }
        }
      }
    });

    client.on("update#cid", async change => {
      if (relations.onchannel.hasOwnProperty(change.to)) {
        try {
          if (
            !blockedChannels.hasOwnProperty(
              relations.onchannel[change.to].config.channel
            )
          ) {
            const resp = await relations.onchannel[change.to].function({
              socket,
              client,
              config: relations.onchannel[change.to].config
            });
            if (resp !== null && resp.hasOwnProperty("interval")) {
              if (resp.interval === true) {
                blockedChannels[
                  relations.onchannel[change.to].config.channel
                ] = null;
                intervals.push({
                  function: relations.onchannel[change.to].function,
                  meta: {
                    socket,
                    client,
                    config: relations.onchannel[change.to].config
                  },
                  interval: resp.delay * 1000,
                  channel: relations.onchannel[change.to].config.channel,
                  lastExecuted: moment()
                });
              }
            }
          }
        } catch (e) {
          console.log(`Error in event: ${relations.onchannel[change.to].functionName} ${e}`)
        }
      }
    });
  }
  for (const event of Object.keys(relations.ingroup)) {
    try {
      await relations.ingroup[event].function({
        socket,
        client: {},
        config: relations.ingroup[event].config
      });
    } catch (e) {
      console.log(`Error in event: ${relations.ingroup[event].functionName} ${e}`)
    }
  }
  setInterval(async () => await socket.clientList({ client_type: 0 }), 250);
})();

const reRunBuilder = async functionName => {
  if (builders.hasOwnProperty(functionName)) {
    const builder = builders[functionName];
    const relationToAdd = await builder.builder({
      config: builder.config
    });
    for (const used of Object.keys(relations[builder.type])) {
      if (relations[builder.type][used].functionName === functionName) {
        delete relations[builder.type][used];
      }
    }
    for (const rel of relationToAdd) {
      const key = Object.keys(rel)[0];
      relations[builder.type][key] = rel[key];
      relations[builder.type][key].functionName = functionName;
    }
  } else {
    console.log("Builder for event " + functionName + " not found");
  }
};

const intervalsExecuter = async () => {
  for (const [index, interval] of intervals.entries()) {
    if (
      moment().diff(interval.lastExecuted, "milliseconds") > interval.interval
    ) {
      try {
        const job = await interval.function(interval.meta);
        if (job.hasOwnProperty("interval")) {
          if (job.interval) {
            interval.lastExecuted = moment();
          }
        } else {
          intervals.splice(index, 1);
          delete blockedChannels[interval.channel];
        }
      } catch (e) {
        console.log(`Error in event: ${interval.functionName} ${e}`)
      }
    }
  }
  setTimeout(async () => {
    await intervalsExecuter();
  }, 100);
};

setTimeout(async () => {
  await intervalsExecuter();
}, 100);

module.exports = { eventExecuter, reRunBuilder };
