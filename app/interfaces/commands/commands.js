const { ts3 } = require('./../../models/teamspeak');
const strategies = require('./strategies/strategies');

const interactions = {};

const socket = ts3();
const commandFactory = async config => {
  try {
    socket.on('textmessage', async e => {
      const client = e.invoker;
      const message = e.msg;
      // czy query
      if (!client.isQuery()) {
        const command = e.msg.trim().toLowerCase();
        // sprawdz czy komenda to help
        if (command === 'help') {
          strategies[command].executer({
            socket,
            message,
            client,
            config,
          });
        } else {
          // sprwadza czy komenda istnieje w strategiach
          if (strategies.hasOwnProperty(command)) {
            // sprwadza czy komenda ma wpis w configu
            if (!config.hasOwnProperty(command)) {
              // nie ma pliku konfiguracyjnego, koniec i wysyla msg do usera
              return await socket.sendTextMessage(
                client.getCache().clid,
                '1',
                `Witaj, [b]${
                client.getCache().client_nickname
                }![/b] Komenda: '[b]${e.msg.trim()}[/b]' nie posiada pliku konfiguracyjnego.`
              );
            }
            // sprawdza czy istnieje interakcja z botem
            if (interactions[client.getDBID()]) {
              // pyta czy wznowic interakcje
              interactions[client.getDBID()].state = 'asking';
              return await socket.sendTextMessage(
                client.getCache().clid,
                '1',
                `Witaj, [b]${
                client.getCache().client_nickname
                }![/b] System wykryl, ze jestes w trakcie schematu komendy: '[b]${
                interactions[client.getDBID()].command
                }[/b]' Chcesz kontynuowac? ( y / n )'.`
              );
            }
            // sprawdzenie czy spelnia wymagania
            let check = false;
            if (config[command].rights.uniqids.includes(client.getUID())) {
              check = true;
            }
            if (
              [
                client.getCache().client_servergroups,
                config[command].rights.groups,
              ].reduce((a, c) => a.filter(i => c.includes(i))).length > 0
            ) {
              check = true;
            }
            if (check !== true) {
              return await socket.sendTextMessage(
                client.getCache().clid,
                '1',
                `Witaj, [b]${
                client.getCache().client_nickname
                }![/b] Nie masz uprawnien do komendy: [b]${command}[/b].`
              );
            }
            // tworzy nowa interakcje
            interactions[client.getDBID()] = {
              state: undefined,
              command: undefined,
              schema: undefined,
            };
            interactions[client.getDBID()].command = command;

            // pobiera schemat komendy
            const tempInteraction = interactions[client.getDBID()];
            const job = await strategies[command].executer({
              socket,
              message,
              client,
              config,
              tempInteraction,
            });
            // inicjalizacja komendy
            if (job.state === 'initialize') {
              interactions[client.getDBID()].schema = job.schema;
              interactions[client.getDBID()].state = job.state;
              // informacja o rozpoczeciu schematu
              await socket.sendTextMessage(
                client.getCache().clid,
                '1',
                `Witaj, [b]${
                client.getCache().client_nickname
                }![/b] Rozpoczynam schemat komendy: [b]${command}[/b].`
              );
              // pytanie o 1 wartosc z schematu
              await socket.sendTextMessage(
                client.getCache().clid,
                '1',
                interactions[client.getDBID()].schema[0].description
              );
            }
            // jesli praca zakonczona sukcesem
            if (job.state === 'success') {
              delete interactions[client.getDBID()];
              return await socket.sendTextMessage(
                client.getCache().clid,
                '1',
                job.message
              );
            }
            // komenda nie istnieje badz podawany jest parametr
          } else {
            // sprawdza czy istnieje interakcja
            if (interactions[client.getDBID()]) {
              // sprawdza czy stan komendy to pytanie o kontynuacje pytania
              if (interactions[client.getDBID()].state === 'asking') {
                // jesli odpowiedz to y - yes
                if (e.msg.trim().toLowerCase() === 'y') {
                  // zmienia stan komendy na progress
                  interactions[client.getDBID()].state = 'progress';
                  // pytam o 1 nie podana wartosc
                  for (const variable of interactions[client.getDBID()].schema) {
                    if (!variable.given) {
                      return await socket.sendTextMessage(
                        client.getCache().clid,
                        '1',
                        variable.description
                      );
                    }
                  }
                  // jesli odpowiedz n - usuwam interakcje
                } else if (e.msg.trim().toLowerCase() === 'n') {
                  return delete interactions[client.getDBID()];
                  // bledna odpowiedz
                } else {
                  return await socket.sendTextMessage(
                    client.getCache().clid,
                    '1',
                    `Nie wiem co z tym zrobic :/`
                  );
                }
                // jesli job state progress albo initalize
              } else if (
                interactions[client.getDBID()].state === 'progress' ||
                interactions[client.getDBID()].state === 'initialize'
              ) {
                let element;
                let index;
                element = '';
                index = '';
                const schemaLength = interactions[client.getDBID()].schema.length;
                for (const [i, variable] of interactions[
                  client.getDBID()
                ].schema.entries()) {
                  if (!variable.given) {
                    element = variable;
                    index = i;
                    break;
                  }
                }
                // informuje o validacji
                await socket.sendTextMessage(
                  client.getCache().clid,
                  '1',
                  `Sprawdzam podany ${element.name}...`
                );
                // rozpoczynam validacje
                const validation = await strategies[
                  interactions[client.getDBID()].command
                ].validator(element.name, e.msg, socket);
                if (validation.state) {
                  // pytanie o validacje pola
                  if (validation.confirm) {
                    for (const msg of validation.message) {
                      await socket.sendTextMessage(
                        client.getCache().clid,
                        '1',
                        msg
                      );
                    }
                    await socket.sendTextMessage(
                      client.getCache().clid,
                      '1',
                      'Wybierz: [b]y[/b] - jesli poprawne, [b]r[/b] - podaj ponownie, [b]n[/b] - zakoncz wykonywanie komendy'
                    );
                    interactions[client.getDBID()].state = 'confirm';
                    element.value = e.msg;
                    interactions[client.getDBID()].schema[index] = element;
                    // validacja bez pytania
                  } else {
                    element.value = e.msg;
                    element.given = true;
                    interactions[client.getDBID()].schema[index] = element;
                    if (schemaLength === index + 1) {
                      const tempInteraction = interactions[client.getDBID()];
                      const job = await strategies[
                        interactions[client.getDBID()].command
                      ].executer({
                        socket,
                        message,
                        client,
                        config,
                        tempInteraction,
                      });
                      if (job.state === 'success') {
                        delete interactions[client.getDBID()];
                        return await socket.sendTextMessage(
                          client.getCache().clid,
                          '1',
                          'Komenda wykonana pomyslnie'
                        );
                      }
                    }
                    for (const variable of interactions[client.getDBID()]
                      .schema) {
                      if (!variable.given) {
                        return await socket.sendTextMessage(
                          client.getCache().clid,
                          '1',
                          variable.description
                        );
                      }
                    }
                  }
                  // wiadomosc o false w validacji
                } else {
                  for (const msg of validation.message) {
                    await socket.sendTextMessage(
                      client.getCache().clid,
                      '1',
                      msg
                    );
                  }
                  return await socket.sendTextMessage(
                    client.getCache().clid,
                    '1',
                    'Podaj jeszcze raz wartosc!'
                  );
                }
                // validacja pola odpowiedz usera
              } else if (interactions[client.getDBID()].state === 'confirm') {
                if (e.msg.trim().toLowerCase() === 'y') {
                  let index;
                  const schemaLength =
                    interactions[client.getDBID()].schema.length;
                  for (const [i, variable] of interactions[
                    client.getDBID()
                  ].schema.entries()) {
                    if (
                      !variable.given &&
                      typeof variable.value !== 'undefined'
                    ) {
                      variable.given = true;
                      index = i;
                      interactions[client.getDBID()].state = 'progress';
                      break;
                    }
                  }

                  if (schemaLength === index + 1) {
                    const tempInteraction = interactions[client.getDBID()];
                    const job = await strategies[
                      interactions[client.getDBID()].command
                    ].executer({
                      socket,
                      message,
                      client,
                      config,
                      tempInteraction,
                    });
                    if (job.state === 'success') {
                      delete interactions[client.getDBID()];
                      return await socket.sendTextMessage(
                        client.getCache().clid,
                        '1',
                        'Komenda wykonana pomyslnie'
                      );
                    }
                  } else {
                    for (const variable of interactions[client.getDBID()]
                      .schema) {
                      if (!variable.given) {
                        return await socket.sendTextMessage(
                          client.getCache().clid,
                          '1',
                          variable.description
                        );
                      }
                    }
                  }
                } else if (e.msg.trim().toLowerCase() === 'r') {
                  for (const variable of interactions[client.getDBID()].schema) {
                    if (
                      !variable.given &&
                      typeof variable.value !== 'undefined'
                    ) {
                      variable.value = undefined;
                      interactions[client.getDBID()].state = 'progress';
                      return await socket.sendTextMessage(
                        client.getCache().clid,
                        '1',
                        variable.description
                      );
                    }
                  }
                } else if (e.msg.trim().toLowerCase() === 'n') {
                  delete interactions[client.getDBID()];
                  return await socket.sendTextMessage(
                    client.getCache().clid,
                    '1',
                    `Anulowanie schematu`
                  );
                } else {
                  return await socket.sendTextMessage(
                    client.getCache().clid,
                    '1',
                    `Nie wiem co z tym zrobic :/`
                  );
                }
              }
              // nie ma interakcji z botem - nie ma takiej komendy
            } else {
              await socket.sendTextMessage(
                client.getCache().clid,
                '1',
                `Witaj, [b]${
                client.getCache().client_nickname
                }![/b] Komenda: '[b]${e.msg.trim()}[/b]' nie istnieje. Lista komend dostepna pod poleceniem '[b]help[/b]'.`
              );
            }
          }
        }
      }

    });
  } catch (e) {
    console.log(e);
  }
};

module.exports = { commandFactory };
