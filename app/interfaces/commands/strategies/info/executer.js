const inf = require("./../../../../version");
module.exports = {
  executer: async ({ socket, message, client, config }) => {
    let userMessage;
    const uptime = format(process.uptime());
    const memory = process.memoryUsage().heapUsed / (1024 * 1024);
    userMessage = `
*****************************
*eBOT
*autor: Mateusz Budaj
*www: https://elipef.pro
*wersja: ${inf.version}
*publikacja: ${inf.release}
******************************
Informacje:
Czas online: ${uptime}
Wykorzystana pamiec: ${memory.toFixed(2)}MB
`;
    return { state: "success", message: userMessage };
  }
};

function format(seconds) {
  function pad(s) {
    return (s < 10 ? "0" : "") + s;
  }
  var hours = Math.floor(seconds / (60 * 60));
  var minutes = Math.floor((seconds % (60 * 60)) / 60);
  var seconds = Math.floor(seconds % 60);

  return pad(hours) + ":" + pad(minutes) + ":" + pad(seconds);
}
