module.exports = {
    helloworld: {
        executer: require('./helloworld/executer').executer,
        validator: require('./helloworld/validator').validator
    },
    guildadd: {
        executer: require('./guildadd/executer').executer,
        validator: require('./guildadd/validator').validator
    },
    guildmusicbot: {
        executer: require('./guildmusicbot/executer').executer,
        validator: require('./guildmusicbot/validator').validator
    },
    guildremove: {
        executer: require('./guildremove/executer').executer,
        validator: require('./guildremove/validator').validator
    },
    guildlist: {
        executer: require('./guildlist/executer').executer,
    },
    pwall: {
        executer: require('./pwall/executer').executer,
        validator: require('./pwall/validator').validator
    },
    channelassign: {
        executer: require('./channelassign/executer').executer,
        validator: require('./channelassign/validator').validator
    },
    channelnocheck: {
        executer: require('./channelnocheck/executer').executer,
        validator: require('./channelnocheck/validator').validator
    },
    pokeall: {
        executer: require('./pokeall/executer').executer,
        validator: require('./pokeall/validator').validator
    },
    help: {
        executer: require('./help/executer').executer,
    },
    info: {
        executer: require('./info/executer').executer,
    }
}