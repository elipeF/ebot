const schema = require('./schema').schema;

module.exports = {
    executer: async ({ socket, message, client, config, tempInteraction }) => {
        if(typeof tempInteraction.schema === 'undefined') {
            return {state: 'initialize', schema: JSON.parse(JSON.stringify(schema))}
        } else {
            return {state: 'success'};
        }
    }
  };
  
