module.exports = {
  schema: [
    {
      name: "clid",
      value: undefined,
      given: false,
      description: "Podaj [b]CLID[/b] uzytkownika"
    },
    {
      name: "uniq",
      value: undefined,
      given: false,
      description: "Podaj [b]UNIQ[/b] uzytkownika"
    }
  ]
};
