const { Guild } = require("./../../../../models/db/guild");

module.exports = {
  executer: async ({ socket, message, client, config }) => {
    let userMessage;
    const guilds = await Guild.find({});
    userMessage = `
Lista gildi(${guilds.length}):
SGID: | NAME:
---------------------`;
    for(const guild of guilds) {
      userMessage = userMessage + `\n${guild.sgid} ${guild.name}`
    }

    return { state: "success", message: userMessage };
  }
};
