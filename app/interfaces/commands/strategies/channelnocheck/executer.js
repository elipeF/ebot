const schema = require("./schema").schema;
const { User } = require("./../../../../models/db/user");
const { PrivateChannel } = require("./../../../../models/db/privatechannel");
const uid = require("uuid/v1");
const moment = require("moment");

module.exports = {
  executer: async ({ socket, message, client, config, tempInteraction }) => {
    if (typeof tempInteraction.schema === "undefined") {
      return {
        state: "initialize",
        schema: JSON.parse(JSON.stringify(schema))
      };
    } else {
      const configS = tempInteraction.schema;
      await PrivateChannel.updateOne({ number: Number(configS[0].value) }, { status: 3 })
      return { state: "success" };
    }
  }
};
