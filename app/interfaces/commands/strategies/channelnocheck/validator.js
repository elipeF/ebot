const moment = require("moment");
const { User } = require("./../../../../models/db/user");
const { PrivateChannel } = require("./../../../../models/db/privatechannel");

module.exports = {
  validator: async (key, value, socket) => {
    if (key === "channelNumber") {
      const findInDb = await PrivateChannel.findOne({ number: Number(value) });
      if (findInDb !== null) {
        if (findInDb.status === 1) {
          const channel = await socket.getChannelByID(findInDb.cid);
          if (typeof channel !== "undefined") {
            const user = await User.findOne({ cldbid: channel.owner });
            if (user !== null) {
              return {
                state: true,
                confirm: true,
                message: [
                  `Kanał, który zostanie zwolniony z sprawdzania daty to: [b]${
                  channel.getCache().channel_name
                  }. Należy on do: ${user.nickname} ostatnio na serwerze: ${moment(user.lastConnection).format("D.MM.YY o H:mm")}`
                ]
              };

            } else {
              return {
                state: true,
                confirm: true,
                message: [
                  `Kanał, który zostanie zwolniony z sprawdzania daty to: [b]${
                  channel.getCache().channel_name
                  }`
                ]
              };
            }
          } else {
            return {
              state: false,
              message: ["Błąd integralności bazy danych. Kanał nie istnieje na serwerze!"]
            };
          }
        } else {
          return {
            state: false,
            message: ["Podany kanał nie jest wolny/zarezerwowany"]
          };
        }
      } else {
        return {
          state: false,
          message: ["Brak kanału o podanym numerze."]
        };
      }
    }
  }
};
