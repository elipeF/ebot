const schema = require("./schema").schema;
const { User } = require("./../../../../models/db/user");
const { PrivateChannel } = require("./../../../../models/db/privatechannel");
const uid = require("uuid/v1");
const moment = require("moment");

module.exports = {
  executer: async ({ socket, message, client, config, tempInteraction }) => {
    if (typeof tempInteraction.schema === "undefined") {
      return {
        state: "initialize",
        schema: JSON.parse(JSON.stringify(schema))
      };
    } else {
      try {
      const configS = tempInteraction.schema;
      const configN = config['channelassign'];
      const channel = await PrivateChannel.findOne({ number: Number(configS[0].value) });
      const hash = uid();
      const channelServer = await socket.getChannelByID(channel.cid);
      const user = await User.findOne({ uniq: configS[1].value });
      const channelName =
        `${channel.number}. kanał uzytkownika - ${
          user.nickname
          }`.length < 40
          ? `${channel.number}. kanał uzytkownika - ${
          user.nickname
          }`
          : `${channel.number}. nowy kanał uzytkownika`;
      await channelServer.edit({
        channel_name: channelName,
        channel_description: configN.newChannelSettings.channelDescriptionUrl.replace(
          "%hash%",
          hash
        ),
        channel_maxclients: -1,
        channel_maxfamilyclients: 0,
        channel_flag_maxclients_unlimited: 1,
        channel_flag_maxfamilyclients_unlimited: 1,
        channel_flag_maxfamilyclients_inherited: 0,
        channel_flag_permanent: 1
      });
      for (i = 1; i <= configN.newChannelSettings.subChannelsCount; i++) {
        await socket.channelCreate(`${i}. Podkanał`, {
          cpid: channel.cid,
          channel_maxclients: -1,
          channel_maxfamilyclients: 0,
          channel_flag_maxclients_unlimited: 1,
          channel_flag_maxfamilyclients_unlimited: 1,
          channel_flag_maxfamilyclients_inherited: 0,
          channel_flag_permanent: 1
        });
      }
      await socket.setClientChannelGroup(
        configN.channelAdmin,
        channel.cid,
        user.cldbid
      );
      await PrivateChannel.updateOne(
        { _id: channel._id },
        {
          hash,
          owner: user.cldbid,
          validTo: moment()
            .add(configN.newChannelSettings.channelValidy, "days")
            .toDate(),
          status: 1,
          createdAt: moment().toDate()
        }
      );
      const client = await socket.getClientByUID(configS[1].value);
      if (typeof client !== "undefined") {
        try {
          await client.move(channel.cid);
        } catch (e) {
          console.log(e);
        }
      } }
      catch(e) {
        console.log(e);
      }
      return { state: "success" };
    } 
  }
};
