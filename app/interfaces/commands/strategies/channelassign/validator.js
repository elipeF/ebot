const moment = require("moment");
const { User } = require("./../../../../models/db/user");
const { PrivateChannel } = require("./../../../../models/db/privatechannel");

module.exports = {
  validator: async (key, value, socket) => {
    if (key === "channelNumber") {
      const findInDb = await PrivateChannel.findOne({ number: Number(value) });
      if (findInDb !== null) {
        if (findInDb.status === 2) {
          const channel = await socket.getChannelByID(findInDb.cid);
          if (typeof channel !== "undefined") {
            return {
              state: true,
              confirm: true,
              message: [
                `Kanał, który zostanie przypisany to: [b]${
                channel.getCache().channel_name
                }`
              ]
            };
          } else {
            return {
              state: false,
              message: ["Błąd integralności bazy danych. Kanał nie istnieje na serwerze!"]
            };
          }
        } else {
          return {
            state: false,
            message: ["Podany kanał nie jest zarezerwowany."]
          };
        }
      } else {
        return {
          state: false,
          message: ["Brak kanału o podanym numerze."]
        };
      }
    } else if (key === "uniq") {
        const userDb = await User.findOne({ uniq: value });
        if (userDb !== null) {
          return {
            state: true,
            confirm: true,
            message: [
              `Podane UNIQ nalezy do: [b]${userDb.nickname}[/b], ktory ostatnio byl na serwerze: [b]${moment(
                userDb.lastConnection
              ).format("D.MM.YY o H:mm")}[/b]`
            ]
          };
        } else {
          return {
            state: false,
            message: ["Uzytkownik nie istnieje w bazie bota."]
          };
        }
      
    }
  }
};
