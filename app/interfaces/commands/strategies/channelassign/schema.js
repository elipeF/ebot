module.exports = {
  schema: [
    {
      name: "channelNumber",
      value: undefined,
      given: false,
      description: "Podaj [b]NUMER[/b] kanału, który ma zostać przypisany do użytkownika"
    },
    {
      name: "uniq",
      value: undefined,
      given: false,
      description: "Podaj [b]UNIQ[/b] użytkownika do którego przypisany bedzie kanał"
    }
  ]
};
