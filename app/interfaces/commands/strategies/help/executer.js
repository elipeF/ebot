module.exports = {
  executer: async ({ socket, message, client, config }) => {
    const clientGroups = client.getCache().client_servergroups;
    const userUniq = client.getUID();
    const commands = [];
    let check;
    for (const command of Object.keys(config)) {
      check = false;
      if (config[command].rights.uniqids.includes(userUniq)) {
        check = true;
      }
      if (
        [clientGroups, config[command].rights.groups].reduce((a, c) =>
          a.filter(i => c.includes(i))
        ).length > 0
      ) {
        check = true;
      }
      if (check === true) {
        commands.push(command);
      }
    }
    if (commands.length > 0) {
      let messageToUser = `\nWitaj, [b]${
        client.getCache().client_nickname
      }![/b] Komendy do ktorych masz dostep:[b]\n`;
      for (const command of commands) {
        messageToUser = messageToUser + `» ${command}\n`;
      }
      try {
        await socket.sendTextMessage(client.getID(), "1", messageToUser);
      } catch (e) {
        console.log(e);
      }
    } else {
      await socket.sendTextMessage(
        client.getID(),
        "1",
        `Witaj, [b]${
          client.getCache().client_nickname
        }![/b] Aktualnie nie ma komendy do ktorej mialbys dostep. Sprobuj ponownie za chwile.'.`
      );
    }
  }
};
