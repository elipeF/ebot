module.exports = {
  validator: async (key, value,socket) => {
    if (value.length > 100) {
      return {
        state: false,
        message: ["Podales za dlugiego poke maksymalna dlugosc to 100"]
      };
    } else if (value.trim.length > 0) {
      return {
        state: false,
        message: ["Podales pusta wiadomosc"]
      };
    } else {
      return {
        state: true,
        confirm: true,
        message: ["Poke, ktory bedzie wyslany do uztykownikow", value]
      };
    }
  }
};
