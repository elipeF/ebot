module.exports = {
  schema: [
    {
      name: "sgid",
      value: undefined,
      given: false,
      description: "Podaj [b]ID[/b] gildii"
    },
    {
      name: "name",
      value: undefined,
      given: false,
      description: "Podaj [b]TAG[/b] gildii"
    },
    {
      name: "dbid",
      value: undefined,
      given: false,
      description: "Podaj [b]DBID[/b] wlasciciela gildii"
    },
    {
      name: "cid_online",
      value: undefined,
      given: false,
      description: "Podaj [b]ID[/b] kanału do wyswietlenia liczby online"
    },
    {
      name: "cid_add",
      value: undefined,
      given: false,
      description: "Podaj [b]ID[/b] kanału DAJ / ZABIERZ range"
    },
    {
      name: "cid_pass",
      value: undefined,
      given: false,
      description: "Podaj [b]ID[/b] kanału na ktory zostanie przerzucony uzytkownik po nadaniu rangi"
    },
  ]
};
