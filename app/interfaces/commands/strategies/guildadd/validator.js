const moment = require("moment");
const { Guild } = require("./../../../../models/db/guild");
const { User } = require("./../../../../models/db/user");

module.exports = {
  validator: async (key, value, socket) => {
    if (key === "sgid") {
      const findInDb = await Guild.findOne({ sgid: Number(value) });
      if (findInDb === null) {
        const group = await socket.getServerGroupByID(value);
        if (typeof group !== "undefined") {
          return {
            state: true,
            confirm: true,
            message: [
              `Grupa, ktora chcesz podpiac ma nazwe: [b]${
                group.getCache().name
              }`
            ]
          };
        } else {
          return {
            state: false,
            message: ["Grupa nie istnieje na serwerze."]
          };
        }
      } else {
        return {
          state: false,
          message: ["Gildia juz wpisana do bazy."]
        };
      }
    } else if (key === "name") {
      return {
        state: true,
        confirm: true,
        message: [`TAG gildii: ${value}`]
      };
    } else if (key === "dbid") {
      const userHasGuild = await Guild.findOne({ dbid: Number(value) });
      if (userHasGuild === null) {
        const userDb = await User.findOne({ cldbid: Number(value) });
        if (userDb !== null) {
          return {
            state: true,
            confirm: true,
            message: [
              `Podane DBID nalezy do: [b]${userDb.nickname}[/b][UNIQ:[b]${
                userDb.uniq
              }[/b]], ktory ostatnio byl na serwerze: [b]${moment(
                userDb.lastConnection
              ).format("D.MM.YY o H:mm")}[/b]`
            ]
          };
        } else {
          return {
            state: false,
            message: ["Uzytkownik nie istnieje w bazie bota."]
          };
        }
      } else {
        return {
          state: false,
          message: ["Uzytkownik posiada juz gildie"]
        };
      }
    } else if (key === "cid_online") {
      const channelHasGuild = await Guild.findOne({
        cid_online: Number(value)
      });
      if (channelHasGuild === null) {
        const channelOnServer = await socket.getChannelByID(Number(value));
        if (typeof channelOnServer !== "undefined") {
          return {
            state: true,
            confirm: true,
            message: [
              `Podane ID to kanał: [b]${
                channelOnServer.getCache().channel_name
              }`
            ]
          };
        } else {
          return {
            state: false,
            message: ["Kanał nie istnieje na serwerze"]
          };
        }
      } else {
        return {
          state: false,
          message: ["Kanał podpiety juz pod jakas gildie"]
        };
      }
    } else if (key === "cid_add") {
      const channelHasGuild = await Guild.findOne({ cid_add: Number(value) });
      if (channelHasGuild === null) {
        const channelOnServer = await socket.getChannelByID(Number(value));
        if (typeof channelOnServer !== "undefined") {
          return {
            state: true,
            confirm: true,
            message: [
              `Podane ID to kanał: [b]${
                channelOnServer.getCache().channel_name
              }`
            ]
          };
        } else {
          return {
            state: false,
            message: ["Kanał nie istnieje na serwerze"]
          };
        }
      } else {
        return {
          state: false,
          message: ["Kanał podpiety juz pod jakas gildie"]
        };
      }
    } else if (key === "cid_pass") {
      const channelHasGuild = await Guild.findOne({ cid_pass: Number(value) });
      if (channelHasGuild === null) {
        const channelOnServer = await socket.getChannelByID(Number(value));
        if (typeof channelOnServer !== "undefined") {
          return {
            state: true,
            confirm: true,
            message: [
              `Podane ID to kanał: [b]${
                channelOnServer.getCache().channel_name
              }`
            ]
          };
        } else {
          return {
            state: false,
            message: ["Kanał nie istnieje na serwerze"]
          };
        }
      } else {
        return {
          state: false,
          message: ["Kanał podpiety juz pod jakas gildie"]
        };
      }
    }
  }
};
