const schema = require("./schema").schema;
const { Guild } = require("./../../../../models/db/guild");
const moment = require("moment");

module.exports = {
  executer: async ({ socket, message, client, config, tempInteraction }) => {
    if (typeof tempInteraction.schema === "undefined") {
      return {
        state: "initialize",
        schema: JSON.parse(JSON.stringify(schema))
      };
    } else {
      const configM = tempInteraction.schema;
      const arrOfGrp = []
      const groupStart = await socket.getServerGroupByID(config.guildadd.range.start);
      const groupEnd = await socket.getServerGroupByID(config.guildadd.range.end);
      if (typeof groupStart === "undefined" || typeof groupEnd === "undefined") {
        await client.message('Błąd podczas określania zakresu.');
        return { state: "success" };
      }
      const startSort = groupStart.getCache().sortid;
      const endSort = groupEnd.getCache().sortid;
      if (Number(endSort) <= Number(startSort)) {
        await client.message('Grupa poczatkowa jest wyzej niz koncowa');
        return { state: "success" };
      }
      const groupServer = await socket.serverGroupList();
      for (const group of groupServer) {
        if (Number(group.getCache().sortid) > Number(startSort) && Number(group.getCache().sortid) < Number(endSort)) {
          arrOfGrp.push(group.getCache().sgid);
        }
      }
      if (arrOfGrp.includes(Number(configM[0].value))) {
        await new Guild({
          sgid: Number(configM[0].value),
          name: configM[1].value,
          dbid: Number(configM[2].value),
          cidOnline: Number(configM[3].value),
          cidAdd: Number(configM[4].value),
          cidPass: Number(configM[5].value),
          createdAt: moment().toDate()
        }).save();
        await require('./../../../events/events').reRunBuilder('guildonline');
        await require('./../../../events/events').reRunBuilder('guildaddremove');
      } else {
        await client.message('Grupa jest z poza przedzialu');
        return { state: "success" };
      }
      return { state: "success" };

    }
  }
};
