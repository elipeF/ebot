const { Guild } = require("./../../../../models/db/guild");
const { MusicBot } = require("./../../../../models/db/musicbot");

module.exports = {
  validator: async (key, value, socket) => {
    if (key === "sgid") {
      const findInDb = await Guild.findOne({ sgid: Number(value) });
      if (findInDb !== null) {
        return {
          state: true,
          confirm: true,
          message: [
            `Podpinasz bota dla gildi: [b]${
            findInDb.name
            }`
          ]
        };
      } else {
        return {
          state: false,
          message: ["Gildia nie istnieje w bazie bota."]
        };
      }
    } else if (key === "cid_bot") {
      const channelHasGuild = await MusicBot.findOne({
        channel: Number(value)
      });
      if (channelHasGuild === null) {
        const channelOnServer = await socket.getChannelByID(Number(value));
        if (typeof channelOnServer !== "undefined") {
          const freeBot = await MusicBot.findOne({ guild: 0 });
          if (freeBot !== null) {
            return {
              state: true,
              confirm: true,
              message: [
                `Podane ID to kanał: [b]${
                channelOnServer.getCache().channel_name
                }`
              ]
            };
          } else {
            return {
              state: false,
              message: ["Brak wolnego bota."]
            };
          }
        } else {
          return {
            state: false,
            message: ["Kanał nie istnieje na serwerze"]
          };
        }
      } else {
        return {
          state: false,
          message: ["Kanał podpiety juz pod jakiegoś bota."]
        };
      }
    }
  }
};
