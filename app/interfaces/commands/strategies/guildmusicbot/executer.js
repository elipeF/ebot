const schema = require("./schema").schema;
const { MusicBot } = require("./../../../../models/db/musicbot");

module.exports = {
  executer: async ({ socket, message, client, config, tempInteraction }) => {
    if (typeof tempInteraction.schema === "undefined") {
      return {
        state: "initialize",
        schema: JSON.parse(JSON.stringify(schema))
      };
    } else {
      const config = tempInteraction.schema;
      await MusicBot.updateOne({ guild: 0 }, { guild: Number(config[0].value), channel: Number(config[1].value) });
      return { state: "success" };
    }
  }
};
