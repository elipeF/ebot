module.exports = {
  schema: [
    {
      name: "sgid",
      value: undefined,
      given: false,
      description: "Podaj [b]ID[/b] gildii"
    },
    {
      name: "cid_bot",
      value: undefined,
      given: false,
      description: "Podaj [b]ID[/b] kanału do nadawania dostępu"
    }
  ]
};
