module.exports = {
  validator: async (key, value, socket) => {
    if (value.length > 1024) {
      return {
        state: false,
        message: ["Podales za dluga wiadomosc maksymalna dlugosc to 1024"]
      };
    } else if (value.trim.length > 0) {
      return {
        state: false,
        message: ["Podales pusta wiadomosc"]
      };
    } else {
      return {
        state: true,
        confirm: true,
        message: ["Wiadomosc, ktora bedzie wyslana do uztykownikow", value]
      };
    }
  }
};
