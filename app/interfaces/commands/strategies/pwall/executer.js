const schema = require("./schema").schema;

module.exports = {
  executer: async ({ socket, message, client, config, tempInteraction }) => {
    if (typeof tempInteraction.schema === "undefined") {
      return {
        state: "initialize",
        schema: JSON.parse(JSON.stringify(schema))
      };
    } else {
      const clients = await socket.clientList({ client_type: 0 });
      const invokerDB = client.getDBID();
      for (const client of clients) {
        if (client.getDBID() !== invokerDB) {
          try {
            await socket.sendTextMessage(client.getID(), "1", tempInteraction.schema[0].value);
          } catch (e) {

          }
        }
      }
      return { state: "success" };
    }
  }
};
