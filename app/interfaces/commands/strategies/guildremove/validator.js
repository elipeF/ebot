const moment = require("moment");
const { Guild } = require("./../../../../models/db/guild");
const { User } = require("./../../../../models/db/user");

module.exports = {
  validator: async (key, value, socket) => {
    if (key === "sgid") {
      const findInDb = await Guild.findOne({ sgid: Number(value) });
      if (findInDb !== null) {
        return {
          state: true,
          confirm: true,
          message: [`Odpinasz gildie o nazwie: [b]${findInDb.name}`]
        };
      } else {
        return {
          state: false,
          message: ["Nie ma takiej gildii w bazie."]
        };
      }
    }
  }
};
