const schema = require("./schema").schema;
const { Guild } = require("./../../../../models/db/guild");

module.exports = {
  executer: async ({ socket, message, client, config, tempInteraction }) => {
    if (typeof tempInteraction.schema === "undefined") {
      return {
        state: "initialize",
        schema: JSON.parse(JSON.stringify(schema))
      };
    } else {
      const config = tempInteraction.schema;
      await Guild.findOneAndDelete({
        sgid: Number(config[0].value)
      });
      await require('./../../../events/events').reRunBuilder('guildonline');
      await require('./../../../events/events').reRunBuilder('guildaddremove');
      return { state: "success" };
    }
  }
};
