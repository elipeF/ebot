const start = async config => {
  const { ts3 } = require("./../models/teamspeak");
  const socket = ts3();
  try {
    await socket.clientUpdate({ client_nickname: config.bot.name });
    console.log("Pomyslnie ustawiono nick");
  } catch (e) {
    if (e.message === "nickname is already in use") {

      process.exit(1);

      //temp disable autoname;

      // let success = false;
      // for (let i = 1; i <= 5; i++) {
      //   try {
      //     await socket.clientUpdate({
      //       client_nickname: config.bot.name + " " + i
      //     });
      //     console.log("Pomyslnie ustawiono nick");
      //     success = true;
      //     break;
      //   } catch (e) {
      //     console.log(e);
      //   }
      // }
      // if (!success) {
      //   console.log("Blad podczas ustawiania nicku");
      //   process.exit(1);
      // }
    } else {
      console.log("error", e.message);
      process.exit(1);
    }
    console.log(e);
  }
  try {
    const whoami = await socket.whoami();
    const client = await socket.getClientByID(whoami.client_id);
    await client.move(config.bot.channel);
    console.log('Bot dolaczyl do kanalu');
  } catch (e) {
    console.log('Bład podczas łączenia do kanału!');
    process.exit(1);
  }
  const path_fs = require("path");
  const fs = require("fs-extra");
  try {
    const path = (await path_fs.resolve()) + "/.app.json";
    await fs.writeJson(path, { pid: process.pid });
  } catch (e) {
    try {
      await socket.quit();
    } catch (e) {
      console.log(e);
    }
    process.exit(1);
  }

};

module.exports = { start };
