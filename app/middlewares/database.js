const mongodb = require("mongodb").MongoClient;

let _db;

const dbConnection = async config => {
  try {
    const socket = await mongodb.connect(
      config.address,
      { useNewUrlParser: true, autoReconnect: false }
    );
    socket.on("close", () => {
      console.log('Connection to database broke');
      process.exit(1);
    })
    _db = socket.db();
    console.log("Connected to database");
  } catch (e) {
    console.log("Can't connect to database!" + e);
    process.exit(1);
  }
};

module.exports.dbConnection = dbConnection;
module.exports.db = () => {
  return _db;
};
// module.exports.db = () => {
//   return _db;
// };
