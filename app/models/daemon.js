const exist = require('path-exists');
const path_fs = require('path');
const fs = require('fs-extra');
const find = require('find-process');
const execa = require('execa');


const bot = require('./bot');

const enable = async () => {
    try {
        const path = await path_fs.resolve() + '/.daemon-settings.json';
        try {
            await exist(path);
            const daemon = await fs.readJson(path);
            await fs.writeJson(path, {status: true, pid: daemon.pid});
            await exec()
        } catch (e) {
            await fs.writeJson(path, {status: true, pid: 0});
            await exec()
        }

    }
    catch (e) {
        console.log(e);
    }
};

const disable = async () => {
    try {
        const path = await path_fs.resolve() + '/.daemon-settings.json';
        try {
            await exist(path);
            const daemon = await fs.readJson(path);
            await fs.writeJson(path, {status: false, pid: daemon.pid});
            await stop()
        } catch (e) {
            await fs.writeJson(path, {status: false, pid: 0});
        }

    }
    catch (e) {
        console.log(e);
    }
};

const exec = async () => {
    try {
        const filename = await path_fs.basename(process.argv[0]);
        const path = await path_fs.resolve();
        try {
            await exist(path + '/.daemon-settings.json');
            const daemon = await fs.readJson(path + '/.daemon-settings.json');
            if (daemon.status) {
                try {
                    const process = await find('pid', daemon.pid);
                    if (typeof process[0] !== "undefined" && process[0].cmd === `./${filename} daemon start`) {
                    } else {
                        await execa.shell(`screen -dmS eBOT-daemon ./${filename} daemon start`, {
                            cwd: path,
                            detached: true
                        });
                    }
                } catch (e) {
                    await execa.shell(`screen -dmS eBOT-daemon ./${filename} daemon start`, {
                        cwd: path,
                        detached: true
                    });
                }
            }
        } catch (e) {
            console.log("First set daemon mode" + e);
        }

    }
    catch (e) {
        console.log(e);
    }
};

const listener = async () => {
    try {
        const path = await path_fs.resolve();
        try {
            await exist(path + '/.daemon-settings.json');
            try {
                const daemon = await fs.readJson(path + '/.daemon-settings.json');
                if (daemon.status) {
                    if (daemon.pid !== process.pid) {
                        try {
                            await fs.writeJson(path + '/.daemon-settings.json', {
                                status: daemon.status,
                                pid: process.pid
                            });
                        } catch (e) {
                            console.log('saving go wrongg' + e);
                        }
                    }
                    try {
                        if (!await bot.running()) {
                            console.log('running');
                            await bot.start();
                        }
                    }
                    catch (e) {
                        console.log(e);
                    }
                } else {
                    process.exit(1);
                }
            } catch (e) {
                if (e.code === "ENOENT") {
                    console.log("First set daemon mode");
                    process.exit(1);
                }
                console.log('Something go wrong' + e);
            }
        } catch (e) {
            console.log("First set daemon mode" + e);
            process.exit(1);
        }
    }
    catch (e) {
        console.log(e);
        process.exit(1);
    }
};


const start = async () => {
    await listener();
};

const stop = async () => {
    try {
        const path = await path_fs.resolve();
        try {
            const filename = await path_fs.basename(process.argv[0]);
            await exist(path + '/.daemon-settings.json');
            const daemon = await fs.readJson(path + '/.daemon-settings.json');

            try {
                const process = await find('pid', daemon.pid);
                if (typeof process[0] !== "undefined" && typeof process[0].cmd !== "undefined" && process[0].cmd === `./${filename} daemon start`) {
                    await fs.writeJson(path + '/.daemon-settings.json', {
                        status: false,
                        pid: -1
                    });

                    await execa.shell(`kill -9 ${daemon.pid}`, {
                        detached: true
                    });

                } else {
                    await fs.writeJson(path + '/.daemon-settings.json', {
                        status: false,
                        pid: -1
                    });
                }
            } catch (e) {

            }

        } catch (e) {
            console.log("First set daemon mode" + e);
            process.exit(1);
        }

    }
    catch (e) {
        console.log(e);
        process.exit(1);
    }
};


module.exports = {enable, disable, exec, start};