const request = require('got');

const api = {}

api.fetchBots = (async (uri) => {
    try {
        const bots = await request.get(uri + '/api/v1/bots', { json: true });
        return { success: true, response: bots.body.bots }
    } catch (e) {
        return { success: false }
    }
});

api.status = (async (uri, id) => {
    try {
        const bot = await request.get(uri + '/api/v1/bots/' + id + '/status', { json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

api.getUsers = (async (uri, id) => {
    try {
        const bot = await request.get(uri + '/api/v1/bots/' + id + '/users', { json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

api.setUsers = (async (uri, id, uniq) => {
    try {
        const bot = await request.post(uri + '/api/v1/bots/' + id + '/users', { body: { userUniqueId: uniq }, json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

api.delUsers = (async (uri, id, uniq) => {
    try {
        const bot = await request.delete(uri + '/api/v1/bots/' + id + '/users', { body: { userUniqueId: uniq }, json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

api.getBotName = (async (uri, id) => {
    try {
        const bot = await request.get(uri + '/api/v1/bots/' + id + '/settings/name', { json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

api.setBotName = (async (uri, id, name) => {
    try {
        const bot = await request.post(uri + '/api/v1/bots/' + id + '/settings/name', { body: { name }, json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

api.getBotChannel = (async (uri, id) => {
    try {
        const bot = await request.get(uri + '/api/v1/bots/' + id + '/settings/channel', { json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

api.setBotChannel = (async (uri, id, cid) => {
    try {
        const bot = await request.post(uri + '/api/v1/bots/' + id + '/settings/channel', { body: { cid }, json: true });
        return { success: true, response: bot.body }
    } catch (e) {
        return { success: false }
    }
});

module.exports = { api }