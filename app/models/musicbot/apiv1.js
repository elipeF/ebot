//New api wrapper to communicate with eMusicBot's
// Developed at 19.08.2019r.

const got = require('got');

//THIS SHOULD BE MOVED
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjhjZTRjNjRjLTcyYTAtNGQ3ZS1hZTFjLTQ1ODI1NmQ1NTA1ZiIsImlhdCI6MTU2NjMxMzU0M30.JKRGcMhlpCQBXEtEU4F5eEewWjbQgYnGAC5agfo_MRU";
const URI = "http://prd-plc-aud01.elipef.pro:8080/api"

const client = got.extend({
    baseUrl: URI,
    headers: {
        'Authorization': `Bearer ${TOKEN}`
    }
});

const api = {}


api.fetchBots = async () => {
    try {
        const req = await client.get('/bot/all', { json: true });
        return { success: true, response: req.body }
    } catch (e) {
        return { success: false }
    }
};

api.fetchBot = async (id) => {
    try {
        const req = await client.get('/bot/' + id, { json: true });
        return { success: true, response: req.body }
    } catch (e) {
        return { success: false }
    }
};

api.getUsers = async (id) => {
    try {
        const req = await client.get('/bot/' + id + '/access', { json: true });
        return { success: true, response: req.body }
    } catch (e) {
        return { success: false }
    }
};


api.setUsers = async (id, uniq) => {
    try {
        const req = await client.post('/bot/' + id + '/access', { body: { useruid: uniq }, json: true });
        return { success: true, response: req.body.data }
    } catch (e) {
        return { success: false }
    }
};

api.delUsers = async (id, uniq) => {
    try {
        const req = await client.delete('/bot/' + id + '/access', { body: { useruid: uniq }, json: true });
        return { success: true, response: req.body.data }
    } catch (e) {
        return { success: false }
    }
};

api.edit = async (id, props) => {
    try {
        const req = await client.patch('/bot/' + id + '/edit', { body: props, json: true });
        return { success: true, response: req.body }
    } catch (e) {
        return { success: false }

    }
}


module.exports = { api }