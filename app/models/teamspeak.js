const TeamSpeak3 = require("ts3-nodejs-library");
const connectionFactory = { ts3: {}, init: {}, stream: {} };
let connectionStatus = false;

const openMessageBox = async client => {
  await connectionFactory.ts3.sendTextMessage(
    client.getID(),
    '1',
    '** eBOT -- COMMANDS INTERFACE -- eBOT **'
  );
};

//Create a new Connection
connectionFactory.init = async (config, channelID) => {
  await new Promise(async resolve => {
    connectionFactory.ts3 = new TeamSpeak3({
      host: config.serverIp,
      protocol: config.queryProto,
      queryport: config.queryPort,
      serverport: config.voicePort,
      username: config.queryLogin,
      password: config.queryPass,
      nickname: "powered by elipef.pro",
      keepalive: true
    });

    connectionFactory.ts3.setMaxListeners(0);
    connectionFactory.ts3.on("error", e => {
      if (e.code === "ECONNREFUSED") {
        console.log("Blad poczas laczenia do serwera");
        process.exit(1);
      } else if (e.id === 1024) {
        console.log(`Brak serwera na porcie: '${config.voicePort}'`);
        process.exit(1);
      } else if (e.id === 520) {
        console.log(`Bledny login lub haslo query`);
        process.exit(1);
      } else if (e.id === 3329) {
        console.log(`Bot query zbanowany za flood!`);
        console.log(
          `Sprobuj ponownie za: ${e.message.replace(/\D/g, "")} sekund`
        );
        process.exit(1);
      } else {
        console.log(e);
      }
      resolve();
    });

    await connectionFactory.ts3.on("flooding", async e => {
      console.log('Bot getting flooded, add IP to query_ip_whitelist');
    });


    //  await connectionFactory.ts3.on("debug", async (e) => {
    //      console.log(e);
    //     // resolve();
    //  });

    await connectionFactory.ts3.on("close", async e => {
      console.log("closed", e);
      process.exit(1);
      resolve();
    });

    await connectionFactory.ts3.on("ready", async () => {
      console.log("Polaczono z serwerem");
      connectionStatus = true;
      await connectionFactory.ts3.registerEvent("server");
      await connectionFactory.ts3.registerEvent("channel", channelID);
      // await connectionFactory.ts3.registerEvent("textserver");
      // await connectionFactory.ts3.registerEvent("textchannel");
      await connectionFactory.ts3.registerEvent("textprivate");
      const clients = await connectionFactory.ts3.clientList({ client_type: 0 });

      for (const client of clients) {
        if (client.getCache().cid === channelID) {
          await openMessageBox(client);
        }
      }

      await connectionFactory.ts3.on('clientmoved', async e => {
        const client = e.client;
        const channel = e.channel;
        if (channel.getCache().cid === channelID) {
          await openMessageBox(client);
        }
      });

      resolve();
    });
  });

  return connectionStatus;
};

connectionFactory.stream = () => {
  return connectionFactory.ts3;
};

// ts3.on("error", e => console.log("Error", e.message));
/*

ts3.registerEvent('')


*/
/*

ts3.on("channeledit", console.log);
ts3.on("channelcreate", console.log);
ts3.on("channelmoved", console.log);
*/

/*
ts3.on("clientmoved", async e => {
    const client = e.client;
    const channel = e.channel;

    if (e.channel.getCache().cid === 2) {
        try {
            await ts3.sendTextMessage(client.getCache().clid, '1', 'Hej, ' + client.getCache().client_nickname);
        } catch (e) {
            console.log(e);
        }
    }
});

*/

/*


ts3.on("ready", async () => {

    let channel = await ts3.channelInfo(2);

    console.log(channel);
    channel.on("connect", console.log);
    //Get all non query clients
/!*    var clients = await ts3.clientList({client_type: 0});

    /!*
        for (const client of clients) {
            client.on("update#client_nickname", change => {
                console.log("Nickname changed", change.from, "=>", change.to)
            })
        }
    *!/

    clients.forEach(client => {
        //subscribe to changes on all clients which had been retrieved
        /!*        console.log("Listening on", client.getCache().client_nickname)

                //event when any property changes on the client
                //this will send an array of changes
                client.on("update", changes => {
                    console.log("Changes on", client.getCache().client_nickname)
                    Object.keys(changes).forEach(k => {
                        console.log(k, changes[k].from, "=>", changes[k].to)
                    })
                })*!/

        //you can also subscribe to a single event
        //this will send an object with the change
        client.on("update#client_nickname", change => {
            console.log("Nickname changed", change.from, "=>", change.to)
        })
    })

    setInterval(() => ts3.clientList(), 1000)*!/
})
*/

//create a check interval in order to cyclic check for changes
//the library will handle the rest of it

//ts3.on("debug", e => console.log(e))s

module.exports = {
  ts3: connectionFactory.stream,
  buildConnection: connectionFactory.init
};
