const path_fs = require("path");
const exist = require("path-exists");
const fs = require("fs-extra");
const hjson = require("hjson");
const read = require("fs-readdir-recursive");

const configs = { commands: {}, events: {}, functions: {} };

const parseConfigs = async () => {
  try {
    const path = (await path_fs.resolve()) + "/configs/";
    for (const conf of await getConfigs()) {
      try {
        const readConf = await fs.readFile(path + conf, "UTF-8");
        try {
          const info = await path_fs.parse(conf);
          if (info.dir === "commands") {
            configs.commands[info.name] = await hjson.parse(readConf);
          } else if (info.dir === "events") {
            configs.events[info.name] = await hjson.parse(readConf);
          } else if (info.dir === "functions") {
            configs.functions[info.name] = await hjson.parse(readConf);
          } else {
            configs[info.name] = await hjson.parse(readConf);
          }
        } catch (e) {
          //TODO warning when failed to parse
        }
      } catch (e) {
        //TODO warning when failed to load file
      }
    }
    if (typeof configs["main"] === "undefined") {
      console.log("main.hjson needed to start");
      process.exit(1);
    }
    console.log('All Configs Parsed');
  } catch (e) {
    console.log(e);
  }
  //console.log(await getConfigs());
};

const getConfigs = async () => {
  try {
    const path = (await path_fs.resolve()) + "/configs/";
    if (await exist(path)) {
      return await read(path);
    } else {
      console.log("Brak katalogu z configami");
      process.exit(1);
    }
  } catch (e) {
    console.log(e);
  }
};

const returnConfigs = async name => {
  if (typeof configs[name] !== "undefined") {
    return configs[name];
  } else {
    return null;
  }
};

const returnKey = async (config, key) => {
  if (typeof config[key] !== "undefined" && config[key] !== null) {
    return config[key];
  } else {
    return false;
  }
};

module.exports = { parseConfigs, configs, returnConfigs, returnKey };
