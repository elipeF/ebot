const { db } = require("../../middlewares/database");

const _db = db();

class Admin {
  constructor(insertObj) {
    this.insertObj = insertObj;
  }

  save() {
    return _db.collection("admins").insertOne(this.insertObj);
  }

  saveMany() {
    return _db.collection("admins").insertMany(this.insertObj);
  }

  static findOneAndDelete(filter) {
    return _db.collection("admins").deleteOne(filter);
  }

  static updateOne(filter, values) {
    return _db.collection("admins").updateOne(filter, {
      $set: values,
      $currentDate: { lastModified: true }
    });
  }

  static findOne(filter) {
    return _db.collection("admins").findOne(filter);
  }

  static getAllSorted(filter) {
    return _db
      .collection("admins")
      .find(filter)
      .sort({ sort: 1 })
      .toArray();
  }

  static find(filter) {
    return _db
      .collection("admins")
      .find(filter)
      .toArray();
  }
}

module.exports = { Admin };

// const mongoose = require("mongoose");
// const moment = require("moment");

// const UserSchema = mongoose.Schema({
//   cldbid: {
//     type: Number,
//     required: true
//   },
//   uniq: {
//     type: String
//   },
//   nickname: {
//     type: String
//   },
//   description: {
//     type: String
//   },
//   totalConnections: {
//     type: Number,
//     default: 1,
//   },
//   lastConnection: {
//     type: Date,
//     default: moment()
//   },
//   firstConnection: {
//     type: Date
//   },
//   longestConnection: {
//     type: Number
//   },
//   timeSpend: {
//     type: Number,
//   },
//   timeSpendOnline: {
//     type: Number,
//   },
//   timeSpendAway: {
//     type: Number
//   },
//   groups: {
//     type: Array
//   },
//   ip: {
//     type: String
//   }
// });

// const User = mongoose.model("User", UserSchema);

// module.exports = { User };
