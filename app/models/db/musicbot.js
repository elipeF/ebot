const { db } = require("./../../middlewares/database");

const _db = db();

class MusicBot {
  constructor(insertObj) {
    this.insertObj = insertObj;
  }

  save() {
    return _db.collection("musicbots").insertOne(this.insertObj);
  }

  saveMany() {
    return _db.collection("musicbots").insertMany(this.insertObj);
  }

  static updateOne(filter, values) {
    return _db.collection("musicbots").updateOne(filter, {
      $set: values,
      $currentDate: { lastModified: true }
    });
  }
  static findOneAndDelete(filter) {
    return _db.collection("musicbots").deleteOne(filter);
  }


  static findOne(filter) {
    return _db.collection("musicbots").findOne(filter);
  }

  static find(filter) {
    return _db
      .collection("musicbots")
      .find(filter)
      .toArray();
  }
}

module.exports = { MusicBot };

// const mongoose = require("mongoose");
// const moment = require("moment");

// const UserSchema = mongoose.Schema({
//   cldbid: {
//     type: Number,
//     required: true
//   },
//   uniq: {
//     type: String
//   },
//   nickname: {
//     type: String
//   },
//   description: {
//     type: String
//   },
//   totalConnections: {
//     type: Number,
//     default: 1,
//   },
//   lastConnection: {
//     type: Date,
//     default: moment()
//   },
//   firstConnection: {
//     type: Date
//   },
//   longestConnection: {
//     type: Number
//   },
//   timeSpend: {
//     type: Number,
//   },
//   timeSpendOnline: {
//     type: Number,
//   },
//   timeSpendAway: {
//     type: Number
//   },
//   groups: {
//     type: Array
//   },
//   ip: {
//     type: String
//   }
// });

// const User = mongoose.model("User", UserSchema);

//module.exports = { User };
