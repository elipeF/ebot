const { db } = require("./../../middlewares/database");

const _db = db();

class PrivateChannel {
  constructor(insertObj) {
    this.insertObj = insertObj;
  }

  save() {
    return _db.collection("privatechannels").insertOne(this.insertObj);
  }

  saveMany() {
    return _db.collection("privatechannels").insertMany(this.insertObj);
  }

  static findOneAndDelete(filter) {
    return _db.collection("privatechannels").deleteOne(filter);
  }

  static updateOne(filter, values) {
    return _db.collection("privatechannels").updateOne(filter, {
      $set: values,
      $currentDate: { lastModified: true }
    });
  }

  static findOne(filter) {
    return _db.collection("privatechannels").findOne(filter);
  }

  static find(filter) {
    return _db
      .collection("privatechannels")
      .find(filter)
      .toArray();
  }
}

module.exports = { PrivateChannel };
// // const PrivateChannelSchema = mongoose.Schema({
// //   cid: {
// //     type: Number,
// //     required: true
// //   },
// //   hash: {
// //     type: String
// //   },
// //   number: {
// //     type: Number
// //   },
// //   owner: {
// //     type: Number
// //   },
// //   validTo: {
// //     type: Date
// //   },
// //   emptySince: {
// //     type: Number
// //   },
// //   status: {
// //     type: Number,
// //     default: 0
// //   },
// //   createdAt: {
// //     type: Date
// //   }
// // });



// module.exports = { PrivateChannel };
