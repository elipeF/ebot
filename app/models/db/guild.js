const { db } = require("../../middlewares/database");

const _db = db();

class Guild {
  constructor(insertObj) {
    this.insertObj = insertObj;
  }

  save() {
    return _db.collection("guilds").insertOne(this.insertObj);
  }

  saveMany() {
    return _db.collection("guilds").insertMany(this.insertObj);
  }

  static findOneAndDelete(filter) {
    return _db.collection("guilds").deleteOne(filter);
  }

  static updateOne(filter, values) {
    return _db.collection("guilds").updateOne(filter, {
      $set: values,
      $currentDate: { lastModified: true }
    });
  }

  static findOne(filter) {
    return _db.collection("guilds").findOne(filter);
  }

  static find(filter) {
    return _db
      .collection("guilds")
      .find(filter)
      .toArray();
  }
}

module.exports = { Guild };

// const mongoose = require("mongoose");
// const moment = require("moment");

// const UserSchema = mongoose.Schema({
//   cldbid: {
//     type: Number,
//     required: true
//   },
//   uniq: {
//     type: String
//   },
//   nickname: {
//     type: String
//   },
//   description: {
//     type: String
//   },
//   totalConnections: {
//     type: Number,
//     default: 1,
//   },
//   lastConnection: {
//     type: Date,
//     default: moment()
//   },
//   firstConnection: {
//     type: Date
//   },
//   longestConnection: {
//     type: Number
//   },
//   timeSpend: {
//     type: Number,
//   },
//   timeSpendOnline: {
//     type: Number,
//   },
//   timeSpendAway: {
//     type: Number
//   },
//   groups: {
//     type: Array
//   },
//   ip: {
//     type: String
//   }
// });

// const User = mongoose.model("User", UserSchema);

// module.exports = { User };
