const { db } = require("../../middlewares/database");

const _db = db();

class Record {
  constructor(insertObj) {
    this.insertObj = insertObj;
  }

  save() {
    return _db.collection("records").insertOne(this.insertObj);
  }

  static updateOne(filter, values) {
    return _db.collection("records").updateOne(filter, {
      $set: values,
      $currentDate: { lastModified: true }
    });
  }

  static findOne(filter) {
    return _db.collection("records").findOne(filter);
  }

  static findLast() {
    return _db.collection("records").findOne({}, { sort: { $natural: -1 } });
  }

  static findXRecords(filter, x) {
    return _db
      .collection("records")
      .find(filter,{ sort: { $natural: -1 } })
      .limit(x)
      .toArray();
  }

  static find(filter) {
    return _db
      .collection("records")
      .find(filter)
      .toArray();
  }
}

module.exports = { Record };

// const mongoose = require("mongoose");
// const moment = require("moment");

// const UserSchema = mongoose.Schema({
//   cldbid: {
//     type: Number,
//     required: true
//   },
//   uniq: {
//     type: String
//   },
//   nickname: {
//     type: String
//   },
//   description: {
//     type: String
//   },
//   totalConnections: {
//     type: Number,
//     default: 1,
//   },
//   lastConnection: {
//     type: Date,
//     default: moment()
//   },
//   firstConnection: {
//     type: Date
//   },
//   longestConnection: {
//     type: Number
//   },
//   timeSpend: {
//     type: Number,
//   },
//   timeSpendOnline: {
//     type: Number,
//   },
//   timeSpendAway: {
//     type: Number
//   },
//   groups: {
//     type: Array
//   },
//   ip: {
//     type: String
//   }
// });

// const User = mongoose.model("User", UserSchema);

// module.exports = { User };
