const exist = require('path-exists');
const path_fs = require('path');
const fs = require('fs-extra');
const find = require('find-process');
const execa = require('execa');


const start = async () => {
    try {
        const filename = await path_fs.basename(process.argv[0]);
        const path = await path_fs.resolve();
        try {
            await execa.shell(`screen -dmS eBOT-app ${filename}`, {
                cwd: path,
                detached: true
            });
        } catch (e) {
            console.log(e);
        }
    } catch (e) {
        console.log(e)
    }
};

const stop = async () => {

};

const running = async () => {
    try {
        const filename = await path_fs.basename(process.argv[0]);
        const path = await path_fs.resolve() + '/.app.json';
        try {
            await exist(path);
            try {
                const bot = await fs.readJson(path);
                const process = await find('pid', bot.pid);
                if (typeof process[0] !== "undefined" && typeof process[0].cmd !== "undefined" && process[0].cmd === `${filename}`) {
                    return true;
                } else {
                    return false;
                }
            } catch (e) {
                console.log(e);
                return false;
            }
        } catch (e) {
            console.log(e);
            return false;
        }
    }
    catch (e) {
        console.log(e);
    }
};


module.exports = {start, stop, running};
